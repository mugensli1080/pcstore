import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IBreadcrumb } from '../core/models/breadcrumb/breadcrumb';
import { DirectoryType } from '../core/models/breadcrumb/directory-type';
import { NAVIGATION_INFO } from '../core/models/breadcrumb/navigation-directories';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { SharedModule } from '../shared/shared.module';
import { AddressComponent } from './components/address/address.component';
import { CheckOutComponent } from './components/check-out/check-out.component';
import { MyOrdersComponent } from './components/my-orders/my-orders.component';
import { OrderSuccessComponent } from './components/order-success/order-success.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { ProductsComponent } from './components/products/products.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { OrderService } from './services/order.service';
import { ShoppingCartService } from './services/shopping-cart.service';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'check-out/:id', component: CheckOutComponent, canActivate: [AuthGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PRODUCTS } } },
            { path: 'my/orders', component: MyOrdersComponent, canActivate: [AuthGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_MY_ORDERS } } },
            { path: 'order-success', component: OrderSuccessComponent, canActivate: [AuthGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_MY_ORDERS } } },
            { path: 'products', component: ProductsComponent, data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_PRODUCTS } } },
            { path: 'product/view/:id', component: ProductViewComponent, data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PRODUCTS } } },
            { path: 'shopping-cart', component: ShoppingCartComponent, data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_SHOPPING_CART } } }
        ])
    ],
    declarations: [
        CheckOutComponent,
        MyOrdersComponent,
        OrderSuccessComponent,
        ProductViewComponent,
        ProductsComponent,
        ShoppingCartComponent,
        AddressComponent,
        ShippingComponent,
        PaymentComponent
    ],
    exports: [
        CheckOutComponent,
        MyOrdersComponent,
        OrderSuccessComponent,
        ProductViewComponent,
        ProductsComponent,
        ShoppingCartComponent,
        AddressComponent,
        ShippingComponent,
        PaymentComponent
    ],
    providers: [
        ShoppingCartService,
        OrderService
    ]
})
export class ClientModule { }