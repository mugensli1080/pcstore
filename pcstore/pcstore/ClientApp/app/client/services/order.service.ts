import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';

import { ISaveOrderResource } from '../../shared/models/save/save-order-resource';
import { IViewOrderResource } from '../../shared/models/view/view-order-resource';

@Injectable()
export class OrderService
{
    private _base = '/api/orders';
    constructor(private _authHttp: AuthHttp) { }

    getOrdersByAuth0User(auth0Id: string): Observable<IViewOrderResource[]>
    {
        return this._authHttp
            .get(this._base + '/user/' + auth0Id)
            .map(res => res.json());
    }

    getOrder(id: number): Observable<IViewOrderResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(order: ISaveOrderResource): Observable<IViewOrderResource>
    {
        return this._authHttp
            .post(this._base, order)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}