import { IViewShoppingCartResource } from '../../shared/models/view/view-shopping-cart-resource';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';

import { ISaveShoppingCartResource } from '../../shared/models/save/save-shopping-cart-resource';


@Injectable()
export class ShoppingCartService
{
    private _base = '/api/shoppingcarts';
    constructor(private _authHttp: AuthHttp) { }

    getUserCarts(auth0Id: string): Observable<IViewShoppingCartResource[]>
    {
        return this._authHttp
            .get(this._base + '/user/' + auth0Id)
            .map(res => res.json());
    }

    getCart(id: number): Observable<IViewShoppingCartResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(cart: ISaveShoppingCartResource): Observable<IViewShoppingCartResource>
    {
        return this._authHttp
            .post(this._base, cart)
            .map(res => res.json());
    }

    update(id: number, cart: ISaveShoppingCartResource): Observable<IViewShoppingCartResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, cart)
            .map(res => res.json());
    }

    delete(id: number): Observable<any>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}