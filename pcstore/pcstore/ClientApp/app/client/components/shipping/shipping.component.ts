import { IViewShippingMethodResource } from '../../../shared/models/view/view-shipping-method-resource';
import { Subscription } from 'rxjs/Rx';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IValidatedGenericForm } from '../../../shared/models/validated-generic-form';
import { IViewShippingCompanyResource } from '../../../shared/models/view/view-shipping-company-resource';
import { ShippingCompanyService } from '../../../shared/services/shipping-company.service';

@Component({
    selector: 'shipping',
    templateUrl: './shipping.component.html',
    styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit, OnDestroy
{
    @Input('shipping') shippingForm: IValidatedGenericForm<number>;
    @Output('on-change') shippingSubmit: EventEmitter<IValidatedGenericForm<number>> = new EventEmitter();

    subscription: Subscription;

    shippingCompanies: IViewShippingCompanyResource[] = [];
    selectedShippingMethod: IViewShippingMethodResource;

    constructor(private _shippingService: ShippingCompanyService) { }

    ngOnInit()
    {
        this.subscription = this._shippingService
            .getShippingCompanies()
            .subscribe(c => this.shippingCompanies = c);
    }

    ngOnDestroy()
    {
        if (this.subscription) this.subscription.unsubscribe();
    }

    onDeliveryMethodSelected(shippingMethod: IViewShippingMethodResource)
    {
        this.selectedShippingMethod = shippingMethod;
        if (this.selectedShippingMethod)
        {
            this.shippingForm.isvalid = true;
            this.shippingForm.form = this.selectedShippingMethod.id;
            this.shippingSubmit.emit(this.shippingForm);
        }
    }
}
