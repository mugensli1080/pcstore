import { IViewSpecificationPhotoResource } from '../../../shared/models/view/view-specification-photo-resource';
import { IViewProductPhotoResource } from '../../../shared/models/view/view-product-photo-resource';
import { IViewAvailabilityResource } from '../../../shared/models/view/view-availability-resource';
import { IProductCard } from '../../../shared/models/product-card';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { ProductService } from '../../../shared/services/product.service';
import * as PATH from './../../../shared/config/path';
import { IViewProductResource } from '../../../shared/models/view/view-product-resource';
import { UploadFolderPath } from './../../../shared/config/path';
import { ICategoryResource } from '../../../shared/models/category-resource';

@Component({
    selector: 'product-view',
    templateUrl: './product-view.component.html',
    styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit
{
    productSubscription: Subscription;
    productCard: IProductCard;
    thumbnail = '';
    category: ICategoryResource;
    hasVideo = false;
    hasSpecification = false;
    productId = 0;
    product: IViewProductResource;

    @ViewChild('video') video: ElementRef;

    constructor(
        private _productService: ProductService,
        private _route: ActivatedRoute,
        private _router: Router
    )
    {
        _route.params.subscribe(p => this.productId = +p['id']);
    }

    ngOnInit(): void
    {
        this.productSubscription = this._productService
            .getProduct(this.productId)
            .subscribe(product =>
            {
                this.product = product;
                let thumbnail = this.product.productPhotos.filter(x => x.activated === true);
                if (thumbnail.length > 0) this.thumbnail = UploadFolderPath.ProductPhotos + thumbnail[0].thumbnail;
                this.productCard = this._productService.createProductCard(this.product);
                this.hasVideo = product.videoLink.length > 0 ? true : false;
                this.hasSpecification = product.specificationPhotos.length > 0 ? true : false;
            });
    }

    get folderPath() { return PATH.UploadFolderPath; }

    play()
    {
        let video = <HTMLVideoElement>this.video.nativeElement;
        return true;
    }
}
