import { Component, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { DexieCartItem } from '../../../shared/models/dexie-cart-item';
import { IProductCard } from '../../../shared/models/product-card';
import { IViewProductResource } from '../../../shared/models/view/view-product-resource';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';
import { ProductService } from '../../../shared/services/product.service';
import { Utilities } from '../../../shared/ultilities/utilities';
import { Page } from '../../../shared/models/page';

export interface IProductCardGroup
{
    group: IProductCard[];
}

@Component({
    selector: 'products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy
{
    isScrolling = false;
    products: IViewProductResource[] = [];
    filteredProducts: IViewProductResource[] = [];
    productCards: IProductCard[] = [];
    productCardGroups: Page<IProductCard>[];
    productSubscription: Subscription;
    shoppingCartServiceSubscription: Subscription;
    category: string | null;

    constructor(private _productService: ProductService,
        private _route: ActivatedRoute,
        private _dexieDbService: DexieDbService,
        @Inject(DOCUMENT) private _document: Document) { }

    ngOnInit(): void
    {
        this.productSubscription = this._productService
            .getProducts()
            .subscribe((products) =>
            {
                this.products = products;
                this._route.queryParamMap
                    .subscribe(params =>
                    {
                        this.category = params.get('category');
                        this.applyFilter();
                        this.populateProductCards(this.filteredProducts);
                    });
            });

    }
    
    @HostListener('window:scroll', []) onWindowScroll($event: any)
    {
        const bodyScroll = this._document.documentElement.scrollTop;
        this.isScrolling = bodyScroll > 20 ? true : false;
    }

    toTop()
    {
        Utilities.toTop(this._document);
    }

    applyFilter(): void
    {
        this.filteredProducts = (this.category)
            ? this.products.filter(p => p.categoryName === this.category)
            : this.products;
    }

    private populateProductCards(products: IViewProductResource[]): void
    {
        this.productCards = [];
        products.forEach(product =>
        {
            let productCard = this._productService.createProductCard(product);
            this.productCards.push(productCard);
        });

        this.productCardGroups = Utilities.paginate(this.productCards, 4);
    }

    private async updateItemDexieDb(item: DexieCartItem): Promise<DexieCartItem | undefined>
    {
        item.quantity++;
        return await this._dexieDbService.updateCartItem(item);
    }

    ngOnDestroy(): void
    {
        if (this.productSubscription)
            this.productSubscription.unsubscribe();

        if (this.shoppingCartServiceSubscription)
            this.shoppingCartServiceSubscription.unsubscribe();
    }
}