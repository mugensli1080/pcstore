import { fade, fadeFast } from '../../../../shared/ng-animations/ng-animations';

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { ICategoryResource } from '../../../../shared/models/category-resource';
import { CategoriesService } from '../../../../shared/services/categories.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute } from '@angular/router/src/router_state';

@Component({
    selector: 'product-filter',
    templateUrl: './product-filter.component.html',
    styleUrls: ['./product-filter.component.scss'],
    animations: [fadeFast]
})
export class ProductFilterComponent implements OnInit, OnDestroy
{
    @Input('category') category: string;

    categories: ICategoryResource[];
    subscription: Subscription;
    selectedCategory: ICategoryResource | null;
    isOpen = false;
    constructor(private _categoriesService: CategoriesService)
    {
    }

    ngOnInit(): void
    {
        this.subscription = this._categoriesService
            .getCategories()
            .subscribe(categories =>
            {
                this.categories = categories;
            });
    }

    ngOnDestroy()
    {
        if (this.subscription) this.subscription.unsubscribe();
    }

    onClicked()
    {
        this.isOpen = !this.isOpen;
    }

    onSelect(category: ICategoryResource)
    {
        this.selectedCategory = category;
    }
}
