import { IViewAddressResource } from '../../../shared/models/view/view-address-resource';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ISaveAddressResource } from '../../../shared/models/save/save-address-resource';
import { NgForm } from '@angular/forms';
import { IValidatedGenericForm } from '../../../shared/models/validated-generic-form';

@Component({
    selector: 'address',
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit
{
    @Input('address') addressForm: IValidatedGenericForm<ISaveAddressResource>;
    @Output('on-change') addressSubmit: EventEmitter<IValidatedGenericForm<ISaveAddressResource>> = new EventEmitter();
    @ViewChild('f') form: NgForm;

    validationKeys: string[] = [];
    constructor() { }

    ngOnInit()
    {
        this.validationKeys = Object.keys(this.addressForm.form);
        this.validationKeys = this.validationKeys.filter(k => k !== 'type');
    }

    save()
    {
        this.addressForm.isvalid = this.form.invalid ? false : true;
        this.addressSubmit.emit(this.addressForm);
    }

}
