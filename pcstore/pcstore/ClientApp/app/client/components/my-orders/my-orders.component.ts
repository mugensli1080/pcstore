import { OrderService } from '../../services/order.service';
import { IViewShoppingCartResource } from '../../../shared/models/view/view-shopping-cart-resource';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { Component, OnInit } from '@angular/core';
import { Auth0UserProfile } from 'auth0-js';
import { IViewOrderResource } from '../../../shared/models/view/view-order-resource';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';

@Component({
    selector: 'my-orders',
    templateUrl: './my-orders.component.html',
    styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit
{
    shoppingCartSubscription: Subscription;
    orderSubscription: Subscription;

    userProfile: Auth0UserProfile;
    shoppingCarts: IViewShoppingCartResource[];
    orders: IViewOrderResource[];

    constructor(private _auth: Auth0Service,
        private _orderService: OrderService,
        private _shoppingCartService: ShoppingCartService) { }

    ngOnInit(): void
    {
        this.userProfile = this._auth.userProfile!;


        this.orderSubscription = this._orderService
            .getOrdersByAuth0User(this.userProfile.user_id)
            .subscribe(orders => this.orders = orders);

        this.shoppingCartSubscription = this._shoppingCartService
            .getUserCarts(this.userProfile!.user_id)
            .subscribe(carts => this.shoppingCarts = carts);
    }

}
