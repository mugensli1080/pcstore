import { OrderService } from '../../services/order.service';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UploadFolderPath } from '../../../shared/config/path';
import { IValidatedGenericForm } from '../../../shared/models/validated-generic-form';
import { IResponsiveTableOptions } from '../../../shared/models/rt-option';
import { ISaveAddressResource } from '../../../shared/models/save/save-address-resource';
import { ISaveOrderResource } from '../../../shared/models/save/save-order-resource';
import { IViewCartItemResource } from '../../../shared/models/view/view-cart-item-resource';
import { IViewShoppingCartResource } from '../../../shared/models/view/view-shopping-cart-resource';
import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';
import { ProductService } from '../../../shared/services/product.service';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { ISaveOrderItemResource } from '../../../shared/models/save/save-order-item-resource';
import { HasToasty } from '../../../shared/models/has-toasty';
import { ToastyService } from 'ng2-toasty';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

type AddressForm = { isvalid: boolean, address: ISaveAddressResource };

@Component({
    selector: 'check-out',
    templateUrl: './check-out.component.html',
    styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit, OnDestroy, HasToasty
{
    orderSubscription: Subscription;
    shoppingCartSubscription: Subscription;

    shoppingCartResource: IViewShoppingCartResource = <IViewShoppingCartResource>{
        id: 0,
        reference: '',
        auth0Id: '',
        email: '',
        cartItems: [],
        totalPrice: 0,
        created: new Date(),
        modified: new Date()
    };

    order: ISaveOrderResource = <ISaveOrderResource>{
        reference: '',
        auth0Id: '',
        email: '',
        addresses: [],
        orderItems: [],
        shippingMethodId: 0,
        paymentMethodId: 0
    };

    invoiceForm: IValidatedGenericForm<ISaveAddressResource> = {
        isvalid: false,
        form: {
            firstName: '',
            lastName: '',
            company: '',
            street: '',
            streetCont: '',
            suburb: '',
            state: '',
            postCode: '',
            country: '',
            type: 'invoice',
        }
    };

    deliveryForm: IValidatedGenericForm<ISaveAddressResource> = {
        isvalid: false,
        form: {
            firstName: '',
            lastName: '',
            company: '',
            street: '',
            streetCont: '',
            suburb: '',
            state: '',
            postCode: '',
            country: '',
            type: 'delivery',
        }
    };

    shippingForm: IValidatedGenericForm<number> = {
        isvalid: false,
        form: 0
    };

    paymentForm: IValidatedGenericForm<number> = {
        isvalid: false,
        form: 0
    }

    items: any[] = [];
    options: IResponsiveTableOptions;

    deliveryOption = 1;
    useInvoiceAddress = false;

    constructor(
        private _shoppingCartService: ShoppingCartService,
        private _orderService: OrderService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _auth: Auth0Service,
        private _toastyService: ToastyService)
    {
        _route.params.subscribe(p => this.shoppingCartResource.id = +p['id']);
    }

    ngOnInit(): void
    {
        this._shoppingCartService
            .getCart(this.shoppingCartResource.id)
            .subscribe(c =>
            {
                this.shoppingCartResource = c;
                this.order.auth0Id = c.auth0Id;
                this.order.email = c.email;
                this.order.reference = c.reference;
                this.setItemsForTable(c.cartItems);
                this.populateCart();
                this.setAddresses();
            });
    }

    ngOnDestroy(): void
    {
        if (this.shoppingCartSubscription) this.shoppingCartSubscription.unsubscribe();
        if (this.orderSubscription) this.orderSubscription.unsubscribe();
    }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 5000
        });
    }
    onToastyError(err: any): void
    {
        console.log(err);
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened!',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
    }

    completeOrder(): void
    {
        this.setOrderAddresses();
        this.setOrderItems();

        this.orderSubscription = this._orderService
            .create(this.order)
            .subscribe(
            order =>
            {
                this.onToastySuccess(order.reference + ' is saved!');
                this._router.navigate(['/my/orders']);
            },
            err => this.onToastyError(err));
    }

    setOrderItems(): void
    {
        this.shoppingCartResource.cartItems.forEach(item =>
        {
            let orderItem = <ISaveOrderItemResource>{
                productId: item.productId,
                quantity: item.quantity
            };
            this.order.orderItems.push(orderItem);
        });
    }

    setOrderAddresses(): void
    {
        this.order.addresses = [];
        if (this.useInvoiceAddress)
        {
            let address = <ISaveAddressResource>{
                firstName: this.invoiceForm.form.firstName,
                lastName: this.invoiceForm.form.lastName,
                company: this.invoiceForm.form.company,
                street: this.invoiceForm.form.street,
                streetCont: this.invoiceForm.form.streetCont,
                suburb: this.invoiceForm.form.suburb,
                state: this.invoiceForm.form.state,
                postCode: this.invoiceForm.form.postCode,
                country: this.invoiceForm.form.country,
                type: 'delivery',
            };
            this.order.addresses.push(address);
        }
        else
        {
            this.order.addresses.push(this.deliveryForm.form);
        }

        this.order.addresses.push(this.invoiceForm.form);
    }

    get isFormReady(): boolean
    {
        let ready = false;

        if (this.deliveryOption === 2)

            if (this.order.paymentMethodId > 0 && this.order.shippingMethodId > 0)
            {
                if (this.useInvoiceAddress)
                    ready = (this.invoiceForm.isvalid) ? false : true;
                else
                    ready = (this.invoiceForm.isvalid && this.deliveryForm.isvalid) ? false : true;
            } else ready = true;

        return ready;
    }

    setAddresses(): void
    {
        this.invoiceForm.form.firstName = this._auth.userProfile.given_name!;
        this.invoiceForm.form.lastName = this._auth.userProfile.family_name!;
        this.deliveryForm.form.firstName = this._auth.userProfile.given_name!;
        this.deliveryForm.form.lastName = this._auth.userProfile.family_name!;
    }

    onShippingChange(shippingForm: IValidatedGenericForm<number>): void
    {
        if (shippingForm.isvalid) this.order.shippingMethodId = shippingForm.form;
    }

    onPaymentChange(paymentForm: IValidatedGenericForm<number>): void
    {
        if (paymentForm.isvalid) this.order.paymentMethodId = paymentForm.form;
    }

    onUseInvoiceAddress(): void
    {
        this.useInvoiceAddress = !this.useInvoiceAddress;
    }

    onInvoiceSubmit(addressForm: IValidatedGenericForm<ISaveAddressResource>): void
    {
        if (addressForm.form.type === 'invoice' && addressForm.isvalid) this.invoiceForm = addressForm;
    }

    onDeliverySubmit(addressForm: IValidatedGenericForm<ISaveAddressResource>)
    {
        if (addressForm.form.type === 'delivery' && addressForm.isvalid) this.deliveryForm = addressForm;
    }

    private setItemsForTable(cartItems: IViewCartItemResource[]): void
    {
        cartItems.forEach(cartItem =>
        {
            this.items.push({
                thumbnail: UploadFolderPath.ProductPhotos + cartItem.thumbnail,
                product: cartItem.productName,
                price: cartItem.unitPrice,
                quantity: cartItem.quantity
            });
        });
    }

    private populateCart(): void
    {
        this.options = {
            hasThumbnail: true,
            thumbnailWidth: '20%',
            hasHeaderColumns: false,
            columns: [
                { name: 'product', isBold: true, isLink: true, isAlignCenter: false },
                { name: 'price', isBold: true, isCurrency: true, isAlignCenter: true },
                { name: 'quantity', isBold: true, fieldWidth: '5%', isAlignCenter: true }
            ],
            items: this.items,
            paging: false,
            search: false
        };
    }
}
