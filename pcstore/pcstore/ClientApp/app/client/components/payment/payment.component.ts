import { PaymentMethodService } from '../../../shared/services/payment-method.service';
import { IViewPaymentMethodResource } from '../../../shared/models/view/view-payment-method-resource';

import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IValidatedGenericForm } from '../../../shared/models/validated-generic-form';
import { ISavePaymentMethodResource } from '../../../shared/models/save/save-payment-resource';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy
{
    @Input('payment') paymentForm: IValidatedGenericForm<number>;
    @Output('on-change') paymentSubmit: EventEmitter<IValidatedGenericForm<number>> = new EventEmitter();
    @ViewChild('f') form: NgForm;
    paymentMethodSubscription: Subscription;

    payments: IViewPaymentMethodResource[];

    selectedPayment: IViewPaymentMethodResource;
    constructor(private _paymentMethodService: PaymentMethodService) { }

    ngOnInit()
    {
        this.paymentMethodSubscription = this._paymentMethodService.getPaymentMethods().subscribe(p => this.payments = p);
    }

    ngOnDestroy()
    {
        if (this.paymentMethodSubscription) this.paymentMethodSubscription.unsubscribe();
    }

    onSelectedPayment(id: number)
    {
        this.selectedPayment = this.payments.filter(x => x.id === parseInt(id + ''))[0];
    }

    save()
    {
        this.paymentForm.isvalid = this.form.invalid ? false : true;
        this.paymentSubmit.emit(this.paymentForm);
    }
}
