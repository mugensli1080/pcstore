import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UUID } from 'angular2-uuid';

import { ISaveCartItemResource } from '../../../shared/models/save/save-cart-item-resource';
import { DexieCartItem } from '../../../shared/models/dexie-cart-item';
import { IResponsiveTableOptions } from '../../../shared/models/rt-option';
import { ISaveShoppingCartResource } from '../../../shared/models/save/save-shopping-cart-resource';
import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';
import { ProductService } from '../../../shared/services/product.service';
import { ShoppingCartService } from '../../services/shopping-cart.service';

import { asEnumerable } from 'linq-es5';
@Component({
    selector: 'shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit
{
    items = <DexieCartItem[]>[];
    options: IResponsiveTableOptions;
    total: number;

    constructor(private _dexieDbService: DexieDbService,
        private _auth: Auth0Service,
        private _router: Router,
        private _shoppingCartService: ShoppingCartService,
        private _productService: ProductService) { }

    async ngOnInit(): Promise<void>
    {
        this.populateCart();
    }

    async onQuantityChange(quantity: number, item: DexieCartItem): Promise<void>
    {
        item.quantity = quantity;
        let result = await this._dexieDbService.updateCartItem(item);
        if (result) this.populateCart();
    }

    async onEmitValue(item: any): Promise<void>
    {
        let result = await this._dexieDbService.updateCartItem(item);
        if (result) this.populateCart();
    }

    removeItem(dexieCartItemId: number): void
    {
        this._dexieDbService.deleteCartItem(dexieCartItemId);
        this.populateCart();
    }

    private async populateCart(): Promise<void>
    {
        this.items = await this._dexieDbService.getAllCartItems();
        this.total = asEnumerable(this.items).Sum(x => x.subTotal);
        this.options = {
            hasThumbnail: true,
            caption: 'Shopping Cart',
            columns: [
                { name: 'productName', isBold: true, isLink: true, isAlignCenter: false },
                { name: 'availability', isBold: true },
                { name: 'price', isBold: true, isCurrency: true },
                { name: 'quantity', isBold: true, isInputField: { number: true, text: false }, fieldWidth: '5%' },
                { name: 'subTotal', isBold: true, isCurrency: true },
                { name: '', isRemovableItem: true }
            ],
            items: this.items,
            paging: false,
            search: false
        };
    }

    clearCart(): void
    {
        this._dexieDbService.removeAllItems();
        this.populateCart();
    }

    async checkOut(): Promise<void>
    {
        if (!this._auth.isAuthenticated())
        {
            localStorage.setItem('current-location', '/shopping-cart');
            this._auth.login();
        }

        let cart = await this.createSaveShoppingCart();
        console.log("checkout: ", JSON.stringify(cart));
        this._shoppingCartService
            .create(cart)
            .subscribe(c =>
            {
                this._dexieDbService.removeAllItems();
                localStorage.removeItem('current-location');
                this._router.navigate(['/check-out', c.id]);
            });

    }

    async createSaveShoppingCart(): Promise<ISaveShoppingCartResource>
    {
        let saveShoppingCartResource = <ISaveShoppingCartResource>{
            auth0Id: this._auth.userProfile.user_id!,
            email: this._auth.userProfile.email!,
            cartItems: await this.createShoppingCartItems()
        };

        return saveShoppingCartResource;
    }

    async createShoppingCartItems(): Promise<ISaveCartItemResource[]>
    {
        let dexieCartItems = await this._dexieDbService.getAllCartItems();
        let cartItems = <ISaveCartItemResource[]>[];
        dexieCartItems.forEach(dexieCartItem =>
        {
            let cartItem = <ISaveCartItemResource>{
                productId: dexieCartItem.productId,
                quantity: dexieCartItem.quantity
            };
            cartItems.push(cartItem);
        });
        return cartItems;
    }
}
