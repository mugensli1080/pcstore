import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IBreadcrumb } from '../core/models/breadcrumb/breadcrumb';
import { DirectoryType } from '../core/models/breadcrumb/directory-type';
import { NAVIGATION_INFO } from '../core/models/breadcrumb/navigation-directories';
import { SharedModule } from '../shared/shared.module';
import { AdminOrdersComponent } from './components/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { AvailabilitiesComponent } from './components/availabilities/availabilities.component';
import { AvailabilityFormComponent } from './components/availability-form/availability-form.component';
import { CategoriesFormComponent } from './components/categories-form/categories-form.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { PaymentMethodFormComponent } from './components/payment-method-form/payment-method-form.component';
import { PaymentMethodsComponent } from './components/payment-methods/payment-methods.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ShippingCompaniesComponent } from './components/shipping-companies/shipping-companies.component';
import { ShippingCompanyFormComponent } from './components/shipping-company-form/shipping-company-form.component';
import { AdminGuardService } from './services/admin-guard.service';
import { AvailabilitiesService } from './services/availabilities.service';


@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'admin/products', component: AdminProductsComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_PRODUCTS_LIST } } },
            { path: 'admin/product/new', component: ProductFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PRODUCTS_LIST } } },
            { path: 'admin/product/:id', component: ProductFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PRODUCTS_LIST } } },

            { path: 'admin/orders', component: AdminOrdersComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_ORDERS } } },

            { path: 'admin/categories', component: CategoriesComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_CATEGORIES } } },
            { path: 'admin/category/new', component: CategoriesFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_CATEGORIES } } },
            { path: 'admin/category/:id', component: CategoriesFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_CATEGORIES } } },

            { path: 'admin/availabilities', component: AvailabilitiesComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_AVAILABILITIES } } },
            { path: 'admin/availability/new', component: AvailabilityFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_AVAILABILITIES } } },
            { path: 'admin/availability/:id', component: AvailabilityFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_AVAILABILITIES } } },

            { path: 'admin/payment-methods', component: PaymentMethodsComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_PAYMENT_METHODS } } },
            { path: 'admin/payment-method/new', component: PaymentMethodFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PAYMENT_METHODS } } },
            { path: 'admin/payment-method/:id', component: PaymentMethodFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_PAYMENT_METHODS } } },

            { path: 'admin/shipping-companies', component: ShippingCompaniesComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.parent, info: NAVIGATION_INFO.DIR_SHIPPING_COMPANIES } } },
            { path: 'admin/shipping-company/new', component: ShippingCompanyFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_SHIPPING_COMPANIES } } },
            { path: 'admin/shipping-company/:id', component: ShippingCompanyFormComponent, canActivate: [AdminGuardService], data: { breadcrumb: <IBreadcrumb>{ type: DirectoryType.child, info: NAVIGATION_INFO.DIR_SHIPPING_COMPANIES } } }
        ])
    ],
    declarations: [
        AdminProductsComponent,
        AdminOrdersComponent,
        ProductFormComponent,
        CategoriesComponent,
        CategoriesFormComponent,
        AvailabilitiesComponent,
        AvailabilityFormComponent,
        PaymentMethodsComponent,
        PaymentMethodFormComponent,
        ShippingCompaniesComponent,
        ShippingCompanyFormComponent
    ],
    exports: [
        AdminProductsComponent,
        AdminOrdersComponent,
        ProductFormComponent,
        CategoriesComponent,
        CategoriesFormComponent,
        AvailabilitiesComponent,
        AvailabilityFormComponent,
        PaymentMethodsComponent,
        PaymentMethodFormComponent,
        ShippingCompaniesComponent,
        ShippingCompanyFormComponent
    ],
    providers: [
        AdminGuardService,
        AvailabilitiesService
    ]
})
export class AdminModule { }