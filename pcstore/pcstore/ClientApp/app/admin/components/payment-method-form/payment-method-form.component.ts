import { Subscription } from 'rxjs/Rx';
import { ISavePaymentMethodResource } from '../../../shared/models/save/save-payment-resource';
import { PaymentMethodService } from '../../../shared/services/payment-method.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HasToasty } from '../../../shared/models/has-toasty';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ToastyService } from 'ng2-toasty';

@Component({
    selector: 'app-payment-method-form',
    templateUrl: './payment-method-form.component.html',
    styleUrls: ['./payment-method-form.component.scss']
})
export class PaymentMethodFormComponent implements OnInit, OnDestroy, HasToasty
{
    paymentMethodId: number;
    subscription: Subscription;

    paymentMethod = <ISavePaymentMethodResource>{
        surcharge: 0
    }
    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _paymentMethodService: PaymentMethodService,
        private _toastyService: ToastyService)
    {
        _route.params.subscribe(p => this.paymentMethodId = +p['id']);
    }

    ngOnInit()
    {
        if (this.paymentMethodId)
            this.subscription = this._paymentMethodService
                .getPaymentMethod(this.paymentMethodId)
                .subscribe(p => this.paymentMethod = p);
    }

    ngOnDestroy()
    {
        if (this.subscription) this.subscription.unsubscribe();
    }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 500
        });
    }

    onToastyError(): void
    {
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened!',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
    }

    save()
    {
        let task$ = (this.paymentMethodId)
            ? this._paymentMethodService.update(this.paymentMethodId, this.paymentMethod)
            : this._paymentMethodService.create(this.paymentMethod);
        task$.subscribe(
            x =>
            {
                this.onToastySuccess(x.name + 'is Saved');
                this._router.navigate(['/admin/payment-method/', x.id])
            },
            err => { this.onToastyError() });
    }

    deletePaymentMethod()
    {
        if (!confirm('Are you sure?')) return;

        this._paymentMethodService
            .delete(this.paymentMethodId)
            .subscribe(() => { this._router.navigate(['/admin/payment-methods']) });

        this.paymentMethod = <ISavePaymentMethodResource>{};
    }
}
