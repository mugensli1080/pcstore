import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { IResponsiveTableOptions } from '../../../shared/models/rt-option';
import { CategoriesService } from '../../../shared/services/categories.service';
import { ICategoryResource } from '../../../shared/models/category-resource';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, OnDestroy
{
    categories: ICategoryResource[];
    categoriesSubscription: Subscription;
    rtoption: IResponsiveTableOptions;
    constructor(
        private _categoriesService: CategoriesService)
    { }

    ngOnInit(): void
    {
        this.categoriesSubscription = this._categoriesService
            .getCategories()
            .subscribe(categories =>
            {
                this.categories = categories;
                this.rtoption = {
                    hasThumbnail: false,
                    columns: [{ name: 'name' }],
                    items: categories,
                    itemLinkUrl: '/admin/category',
                    paging: true,
                    search: true
                };
            });

    }

    ngOnDestroy(): void
    {
        if (this.categoriesSubscription) this.categoriesSubscription.unsubscribe();
    }
}
