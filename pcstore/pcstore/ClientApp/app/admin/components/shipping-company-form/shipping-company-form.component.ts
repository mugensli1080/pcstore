import { ISaveShippingMethodResource } from '../../../shared/models/save/save-shipping-method-resource';
import { HasToasty } from '../../../shared/models/has-toasty';
import { Subscription } from 'rxjs/Rx';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ISaveShippingCompanyResource } from '../../../shared/models/save/save-shipping-company-resource';
import { ShippingCompanyService } from '../../../shared/services/shipping-company.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IViewShippingCompanyResource } from '../../../shared/models/view/view-shipping-company-resource';
import { ShippingMethodService } from '../../../shared/services/shipping-method.service';
import { IViewShippingMethodResource } from '../../../shared/models/view/view-shipping-method-resource';
import { ToastyService } from 'ng2-toasty';

@Component({
    selector: 'app-shipping-company-form',
    templateUrl: './shipping-company-form.component.html',
    styleUrls: ['./shipping-company-form.component.scss']
})
export class ShippingCompanyFormComponent implements OnInit, OnDestroy, HasToasty
{

    shippingCompanySubscription: Subscription;
    shippingMethodSubscription: Subscription;

    shippingCompanyId: number;
    shippingCompany = <ISaveShippingCompanyResource>{
        company: '',
        shippingMethods: [],
        slogan: '',
        specialNote: ''
    };

    shippingMethod = <ISaveShippingMethodResource>{
        deliveryMethod: '',
        estimateTime: '',
        price: 0
    };

    shippingMethods: IViewShippingMethodResource[];

    constructor(private _shippingCompanyService: ShippingCompanyService,
        private _shippingMethodService: ShippingMethodService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _toastyService: ToastyService)
    {
        _route.params.subscribe(p => this.shippingCompanyId = +p['id']);
    }

    ngOnInit()
    {
        if (this.shippingCompanyId)
            this.shippingCompanySubscription = this._shippingCompanyService
                .getShippingCompany(this.shippingCompanyId)
                .subscribe(s => this.shippingCompany = s);

        this.shippingMethodSubscription = this._shippingMethodService
            .getShippingMethods()
            .subscribe(shippingMethods => this.shippingMethods = shippingMethods);
    }

    ngOnDestroy()
    {
        if (this.shippingCompanySubscription) this.shippingCompanySubscription.unsubscribe();
    }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 5000
        });
    }
    onToastyError(): void
    {
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
    }

    onAddShippingMethod()
    {
        if (this.shippingMethod.deliveryMethod && this.shippingMethod.price && this.shippingMethod.estimateTime)
        {
            this.shippingCompany.shippingMethods.push(this.shippingMethod);
            this.shippingMethod = <ISaveShippingMethodResource>{
                deliveryMethod: '',
                price: 0,
                estimateTime: ''
            };
        }
    }

    save()
    {
        console.log("shipping company: ", JSON.stringify(this.shippingCompany));
        let task$ = (this.shippingCompanyId)
            ? this._shippingCompanyService.update(this.shippingCompanyId, this.shippingCompany)
            : this._shippingCompanyService.create(this.shippingCompany);
        task$.subscribe(
            shippingCompany =>
            {
                this.onToastySuccess(shippingCompany.company + ' is saved!');
                this._router.navigate(['/admin/shipping-company/', shippingCompany.id]);
            },
            err => this.onToastyError())
    }

    deleteShippingcompany()
    {
        if (!confirm('Are you sure?')) return;
        this._shippingCompanyService
            .delete(this.shippingCompanyId)
            .subscribe(
            id =>
            {
                this.onToastySuccess('Company ' + id + ' is deleted!')
                this._router.navigate(['/admin/shipping-companies']);
            },
            err => this.onToastyError());
    }
}
