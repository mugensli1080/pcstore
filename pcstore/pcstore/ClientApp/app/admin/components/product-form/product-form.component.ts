import { HasToasty } from '../../../shared/models/has-toasty';
import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { selector } from 'rxjs/operator/publish';

import { UploadFolderPath } from '../../../shared/config/path';
import { ICategoryResource } from '../../../shared/models/category-resource';
import { ISaveProductResource } from '../../../shared/models/save/save-product-resource';
import { ISubCategoryResource } from '../../../shared/models/sub-categetory-resource';
import { IViewAvailabilityResource } from '../../../shared/models/view/view-availability-resource';
import { IViewProductPhotoResource } from '../../../shared/models/view/view-product-photo-resource';
import { IViewProductResource } from '../../../shared/models/view/view-product-resource';
import { CategoriesService } from '../../../shared/services/categories.service';
import { PhotoService, PhotoType } from '../../../shared/services/photo.service';
import { ProductService } from '../../../shared/services/product.service';
import { Progress, ProgressService } from '../../../shared/services/progress.service';
import { SubCategoriesService } from '../../../shared/services/sub-categories.service';
import { AvailabilitiesService } from '../../services/availabilities.service';
import { ToastyService } from 'ng2-toasty';


@Component({
    selector: 'app-product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy, HasToasty
{
    @ViewChild('productPhotoInput') productPhotoInput: ElementRef;
    @ViewChild('specPhotoInput') specPhotoInput: ElementRef;
    uploadFolderPath = { ProductPhotos: '', SpecificationPhotos: '' };

    productId = 0;
    product = <IViewProductResource>{
        id: 0,
        name: '',
        description: '',
        price: 0,
        stock: 1,
        availabilityId: 0,
        availabilityName: '',
        categoryId: 0,
        categoryName: '',
        subCategoryId: 0,
        subCategoryName: '',
        productPhotos: [],
        specificationPhotos: [],
        videoLink: '',
        created: new Date(),
        modified: new Date()
    };

    productSubscription: Subscription;
    photoSubscription: Subscription;
    categoriesSubscription: Subscription;
    availabilityScription: Subscription;

    categories: ICategoryResource[];
    subCategories: ISubCategoryResource[];
    availabilities: IViewAvailabilityResource[];

    productPhoto: IViewProductPhotoResource | null;
    progress: Progress | null = { total: 0, percentage: 0 };
    progressTab: number;

    constructor(
        private _productService: ProductService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _progressService: ProgressService,
        private _zone: NgZone,
        private _categoriesService: CategoriesService,
        private _subCategoriesService: SubCategoriesService,
        private _photoService: PhotoService,
        private _availabilitiesService: AvailabilitiesService,
        private _toastyService: ToastyService
    )
    {
        _route.params.subscribe(p => this.productId = +p['id']);
        this.uploadFolderPath = UploadFolderPath;
    }

    ngOnInit(): void
    {
        if (this.productId)
            this.productSubscription = this._productService
                .getProduct(this.productId)
                .subscribe(product =>
                {
                    this.product = product;
                    this.productPhoto = product.productPhotos.filter(x => x.activated === true)[0];
                    this.categoryChange(product.categoryId);
                });

        this.categoriesSubscription = this._categoriesService
            .getCategories()
            .subscribe(c =>
            {
                this.categories = c;
                if (!this.product.id)
                {
                    this.product.categoryId = c[0].id!;
                    this.categoryChange(this.product.categoryId);
                    this.product.subCategoryId = c[0].subCategories[0].id!;
                    console.log(this.product.subCategoryId);
                }
            });
        this.availabilityScription = this._availabilitiesService
            .getAvailabilities()
            .subscribe(a =>
            {
                this.availabilities = a;
                if (!this.productId) this.product.availabilityId = a[0].id!;
            });
    }

    ngOnDestroy(): void
    {
        if (this.productSubscription) this.productSubscription.unsubscribe();
        if (this.photoSubscription) this.photoSubscription.unsubscribe();
        if (this.categoriesSubscription) this.categoriesSubscription.unsubscribe();
        if (this.availabilityScription) this.availabilityScription.unsubscribe();
    }

    categoryChange(id: number): void
    {
        this._subCategoriesService.getSubCategories(id);
        this._categoriesService
            .getCategory(id)
            .subscribe(c =>
            {
                this.subCategories = c.subCategories;
                this.product.subCategoryId = c.subCategories[0].id;
            });
    }

    save(): void
    {
        let saveProduct = <ISaveProductResource>{
            name: this.product.name,
            description: this.product.description,
            stock: this.product.stock,
            price: this.product.price,
            availabilityId: this.product.availabilityId,
            subCategoryId: this.product.subCategoryId,
            videoLink: this.product.videoLink
        };

        let result$ = (this.product.id)
            ? this._productService.update(this.product.id, this.product)
            : this._productService.create(this.product);

        result$.subscribe(
            p =>
            {
                this.onToastySuccess(p.name + ' is saved!');
                this._router.navigate(['/admin/product/', p.id]);
            },
            err => this.onToastyError()
        );
    }

    onToastyError()
    {
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened!',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        })
    }

    onToastySuccess(msg: string)
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 5000
        });
    }

    deleteProduct(): void
    {
        if (!confirm('Are you sure?')) return;

        this._productService
            .delete(this.product.id)
            .subscribe(
            id =>
            {
                this.onToastySuccess('Product ' + id + ' is deleted!');
                this._router.navigate(['/admin/products']);
            },
            err => this.onToastyError()
            );

        this.product = <IViewProductResource>{};
    }

    deleteProductPhoto(id: number): void
    {
        if (!confirm('Are you sure?')) return;
        this._photoService
            .delete(this.product.id, id, PhotoType.Product)
            .subscribe(() =>
            {
                this.product.productPhotos = this.product.productPhotos.filter(x => x.id !== id);
            });
    }

    deleteSpecificationPhoto(id: number): void
    {
        if (!confirm('Are you sure?')) return;
        this._photoService
            .delete(this.product.id, id, PhotoType.Specification)
            .subscribe(() =>
            {
                this.product.specificationPhotos = this.product.specificationPhotos.filter(x => x.id !== id);
            });
    }

    uploadProductPhoto(): void
    {
        let nativeElement: HTMLInputElement = this.productPhotoInput.nativeElement;
        this.startProgressBar(1);
        this._photoService
            .upload(this.product.id, PhotoType.Product, nativeElement!.files![0])
            .subscribe(photo =>
            {
                this.product.productPhotos.push(photo);
                nativeElement.value = '';
            });
    }

    activateProductPhoto(productPhoto: IViewProductPhotoResource): void
    {
        this.productPhoto = productPhoto;
        this._photoService
            .activateProductPhoto(this.product.id, this.productPhoto.id)
            .subscribe(p => this.setProductPhotos(p));
    }

    setProductPhotos(photo: IViewProductPhotoResource)
    {
        this.product.productPhotos.filter(p =>
        {
            p.activated = false;
            if (p.id === photo.id) p.activated = true;
            return p;
        });
    }

    uploadSpecPhoto(): void
    {
        let nativeElement: HTMLInputElement = this.specPhotoInput.nativeElement;
        this.startProgressBar(2);
        this._photoService
            .upload(this.product.id, PhotoType.Specification, nativeElement!.files![0])
            .subscribe(photo =>
            {
                this.product.specificationPhotos.push(photo);
                nativeElement.value = '';
            });
    }

    startProgressBar(tabNumber: number): void
    {
        this.progressTab = tabNumber;
        this._progressService
            .startTracking()
            .subscribe(
            progress => { this._zone.run(() => { this.progress = progress; }) },
            error => { console.log("error, tracking progress: " + error) },
            () => { this.progress = null; }
            );
    }

}
