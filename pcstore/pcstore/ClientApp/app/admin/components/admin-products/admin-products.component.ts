import { UploadFolderPath } from '../../../shared/config/path';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

import { IResponsiveTableOptions, IRTColumnOption } from '../../../shared/models/rt-option';
import { CategoriesService } from '../../../shared/services/categories.service';
import { ProductService } from '../../../shared/services/product.service';
import { SubCategoriesService } from '../../../shared/services/sub-categories.service';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';
import { IViewProductResource } from '../../../shared/models/view/view-product-resource';

@Component({
    selector: 'admin-products',
    templateUrl: './admin-products.component.html',
    styleUrls: ['./admin-products.component.scss']
})
export class AdminProductsComponent implements OnInit, OnDestroy
{
    products: IViewProductResource[];
    productSubscription: Subscription;
    options: IResponsiveTableOptions = <IResponsiveTableOptions>{
        caption: '',
        columns: <IRTColumnOption[]>[{}],
        items: <any[]>[],
        hasThumbnail: false,
        itemLinkUrl: '',
        paging: true,
        search: true
    };

    constructor(
        private _productService: ProductService,
        private _categoriesService: CategoriesService,
        private _dexieDbService: DexieDbService,
        private _subCategoriesService: SubCategoriesService) { }

    ngOnInit()
    {
        this.productSubscription = this._productService
            .getProducts()
            .subscribe(products =>
            {
                this.products = products;
                this.setOptionsItems();
                this.setOptions();
            });
    }

    ngOnDestroy(): void
    {
        if (this.productSubscription) this.productSubscription.unsubscribe();
    }

    async setOptions(): Promise<void>
    {
        this.options.caption = '';
        this.options.hasThumbnail = true;
        this.options.columns = [
            { name: 'name' },
            { name: 'description' },
            { name: 'stock' },
            { name: 'availability' },
            { name: 'category' },
            { name: 'subCategory' }
        ];
        this.options.itemLinkUrl = '/admin/product';
        this.options.paging = true;
        this.options.search = true;
    }

    setOptionsItems(): void
    {
        let items = <any[]>[];
        this.products.forEach(product =>
        {
            let thumbnail = '';
            let photo = product.productPhotos.filter(p => p.activated === true)[0];
            if (photo) thumbnail = UploadFolderPath.ProductPhotos + photo.thumbnail;

            let item = {
                thumbnail: thumbnail,
                id: product.id,
                name: product.name,
                description: product.description,
                stock: product.stock,
                availability: product.availabilityName,
                category: product.categoryName,
                subCategory: product.subCategoryName
            };
            items.push(item);
        });
        this.options.items = items;
    }
}