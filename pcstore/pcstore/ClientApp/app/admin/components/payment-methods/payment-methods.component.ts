import { IResponsiveTableOptions, IRTColumnOption } from '../../../shared/models/rt-option';
import { Subscription } from 'rxjs/Rx';
import { PaymentMethodService } from '../../../shared/services/payment-method.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { IViewPaymentMethodResource } from '../../../shared/models/view/view-payment-method-resource';

@Component({
    selector: 'payment-methods',
    templateUrl: './payment-methods.component.html',
    styleUrls: ['./payment-methods.component.scss']
})
export class PaymentMethodsComponent implements OnInit, OnDestroy
{
    subscription: Subscription;
    paymentMethods: IViewPaymentMethodResource[];
    options: IResponsiveTableOptions;

    constructor(private _paymentMethodService: PaymentMethodService) { }

    ngOnInit()
    {
        this.subscription = this._paymentMethodService
            .getPaymentMethods()
            .subscribe(paymentMethods =>
            {
                this.paymentMethods = paymentMethods;
                this.options = <IResponsiveTableOptions>{
                    caption: '',
                    columns: [
                        { name: 'id', isAlignCenter: true },
                        { name: 'name', isAlignCenter: true },
                        { name: 'surcharge', isAlignCenter: true },
                        { name: 'important', isAlignCenter: true },
                        { name: 'note', isAlignCenter: true }
                    ],
                    items: paymentMethods,
                    hasThumbnail: false,
                    itemLinkUrl: '/admin/payment-method',
                    paging: true,
                    search: true
                };
            });
    }

    ngOnDestroy()
    {
        if (this.subscription) this.subscription.unsubscribe();
    }
}
