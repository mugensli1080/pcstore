import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { IResponsiveTableOptions } from '../../../shared/models/rt-option';
import { IViewAvailabilityResource } from '../../../shared/models/view/view-availability-resource';
import { AvailabilitiesService } from '../../services/availabilities.service';

@Component({
    selector: 'app-availabilities',
    templateUrl: './availabilities.component.html',
    styleUrls: ['./availabilities.component.scss']
})
export class AvailabilitiesComponent implements OnInit, OnDestroy
{
    availabilities: IViewAvailabilityResource[];
    subscription: Subscription;
    rtoption: IResponsiveTableOptions;

    constructor(private _availabilitiesService: AvailabilitiesService) { }

    ngOnInit(): void
    {
        this.subscription = this._availabilitiesService
            .getAvailabilities()
            .subscribe(availabilities =>
            {
                this.availabilities = availabilities;
                this.rtoption = {
                    hasThumbnail: false,
                    columns: [{ name: 'name' }, { name: 'created' }, { name: 'modified' }],
                    items: availabilities,
                    itemLinkUrl: '/admin/availability',
                    paging: true,
                    search: true
                }
            });
    }

    ngOnDestroy(): void
    {
        if (this.subscription) this.subscription.unsubscribe();
    }
}
