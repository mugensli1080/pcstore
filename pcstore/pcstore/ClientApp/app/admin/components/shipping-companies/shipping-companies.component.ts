import { ShippingCompanyService } from '../../../shared/services/shipping-company.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { IViewShippingCompanyResource } from '../../../shared/models/view/view-shipping-company-resource';
import { IResponsiveTableOptions } from '../../../shared/models/rt-option';

@Component({
    selector: 'app-shipping-companies',
    templateUrl: './shipping-companies.component.html',
    styleUrls: ['./shipping-companies.component.scss']
})
export class ShippingCompaniesComponent implements OnInit, OnDestroy
{
    subscription: Subscription;
    shippingCompanies: IViewShippingCompanyResource[];
    options: IResponsiveTableOptions;
    items: any[] = [];
    constructor(private _shippingCompanyService: ShippingCompanyService) { }

    ngOnInit()
    {
        this.subscription = this._shippingCompanyService
            .getShippingCompanies()
            .subscribe(shippingCompanies =>
            {
                this.shippingCompanies = shippingCompanies;
                this.setItems();
                this.options = <IResponsiveTableOptions>{
                    caption: '',
                    columns: [
                        { name: 'company', isAlignCenter: true },
                        { name: 'slogan', isAlignCenter: true },
                        { name: 'specialNote', isAlignCenter: true },
                    ],
                    items: this.items,
                    hasThumbnail: true,
                    itemLinkUrl: '/admin/shipping-company',
                    paging: true,
                    search: true
                };
            });
    }

    ngOnDestroy()
    {
        if (this.subscription) this.subscription.unsubscribe();
    }

    setItems()
    {
        this.shippingCompanies.forEach(s =>
        {
            this.items.push({
                thumbnail: s.productPhoto ? s.productPhoto.thumbnail! : '',
                id: s.id,
                company: s.company,
                slogan: s.slogan,
                specialNote: s.specialNote
            });
        });
    }

}
