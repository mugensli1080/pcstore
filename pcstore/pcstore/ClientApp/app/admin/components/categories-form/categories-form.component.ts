import { HasToasty } from '../../../shared/models/has-toasty';
import { OnDestroy, ViewChild } from '@angular/core';
import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { CategoriesService } from '../../../shared/services/categories.service';
import { SubCategoriesService } from '../../../shared/services/sub-categories.service';
import { ICategoryResource } from '../../../shared/models/category-resource';
import { ISubCategoryResource } from '../../../shared/models/sub-categetory-resource';
import { ToastyService } from 'ng2-toasty';
import { NgModel } from '@angular/forms';

@Component({
    selector: 'categories-form',
    templateUrl: './categories-form.component.html',
    styleUrls: ['./categories-form.component.scss']
})
export class CategoriesFormComponent implements OnInit, OnDestroy, HasToasty
{
    @ViewChild('newSubCategory') newSubCategory: ElementRef;
    @ViewChild('categoryName') inputCategory: NgModel;
    category: ICategoryResource = <ICategoryResource>{
        name: '',
        subCategories: []
    };
    subCategories: ISubCategoryResource[] = <ISubCategoryResource[]>[{}];
    fileredSubcategories: ISubCategoryResource[];
    categoriesSubscription: Subscription;
    subCategoriesSubscription: Subscription;
    selectedSubCategory: ISubCategoryResource = <ISubCategoryResource>{
        categoryId: 0,
        id: 0,
        name: ''
    };
    isDisabled = false;

    constructor(
        private _categoriesService: CategoriesService,
        private _subCategoriesService: SubCategoriesService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _toastyService: ToastyService)
    {
        _route.params.subscribe(p =>
        {
            if (+p['id'] > 0) this.category.id = +p['id'];
        });
    }

    ngOnInit(): void
    {
        if (this.category.id)
            this.categoriesSubscription = this._categoriesService
                .getCategory(this.category.id)
                .subscribe(c =>
                {
                    this.category = c;
                    this.subCategoriesSubscription = this._subCategoriesService
                        .getSubCategories(this.category.id!)
                        .subscribe(s => this.subCategories = this.fileredSubcategories = s.filter(x => x.categoryId === this.category.id));
                });

    }

    ngOnDestroy(): void
    {
        if (this.categoriesSubscription) this.categoriesSubscription.unsubscribe();
        if (this.subCategoriesSubscription) this.subCategoriesSubscription.unsubscribe();
    }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 5000
        });
    }
    onToastyError(): void
    {
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened!',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
    }

    onSearch(param: string): void
    {
        this.fileredSubcategories = this.subCategories.filter(s => s.name.toLowerCase().includes(param.toLowerCase()));
    }

    saveCategory(): void
    {
        console.log("category: ", this.category);
        let task$ = this.category.id
            ? this._categoriesService.update(this.category.id, this.category)
            : this._categoriesService.create(this.category);
        task$.subscribe(
            category =>
            {
                this.onToastySuccess(category.name + ' is saved!');
                this._router.navigate(['/admin/category/', category.id]);
            },
            err => this.onToastyError()
        );
    }

    deleteCategory(): void
    {
        if (!confirm('Are you sure?')) return;
        this._categoriesService
            .delete(this.category.id!)
            .subscribe(
            id =>
            {
                this.onToastySuccess('Category ' + id + ' is deleted.');
                this._router.navigate(['/admin/categories']);
            },
            err => this.onToastyError()
            );
    }

    saveSubCategory(): void
    {
        let newSubCategory = <ISubCategoryResource>{
            categoryId: this.category.id,
            id: 0,
            name: this.newSubCategory.nativeElement.value
        };

        let task$ = this.selectedSubCategory.id
            ? this._subCategoriesService.update(this.selectedSubCategory.categoryId, this.selectedSubCategory)
            : this._subCategoriesService.create(newSubCategory.categoryId, newSubCategory);

        task$.subscribe(
            subCateggory =>
            {
                this.onToastySuccess(subCateggory.name + ' is saved!');

                if (!this.subCategories.find(x => x.id === subCateggory.id))
                    this.subCategories.push(subCateggory);

                this.fileredSubcategories = this.subCategories;
                this.newSubCategory.nativeElement.value = '';
                this.clearEdit();
            },
            err => this.onToastyError());
    }

    editSubCategory(subCategory: ISubCategoryResource): void
    {
        this.selectedSubCategory = subCategory;
        this.isDisabled = true;
    }

    deleteSubCategory(): void
    {
        if (!confirm('Are you sure?')) return;
        this._subCategoriesService
            .delete(this.selectedSubCategory.categoryId, this.selectedSubCategory.id)
            .subscribe(id =>
            {
                this.onToastySuccess('Sub category ' + id + ' is deleted!');
                this.subCategories = this.subCategories.filter(s => s.id != id);
                this.fileredSubcategories = this.subCategories;
                this.clearEdit();
            });
    }

    clearEdit(): void
    {
        this.selectedSubCategory = { categoryId: 0, id: 0, name: '' };
        this.isDisabled = false;
    }
}
