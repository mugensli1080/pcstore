import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { AvailabilitiesService } from '../../services/availabilities.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IViewAvailabilityResource } from '../../../shared/models/view/view-availability-resource';
import { HasToasty } from '../../../shared/models/has-toasty';
import { ToastyService } from 'ng2-toasty';

@Component({
    selector: 'app-availability-form',
    templateUrl: './availability-form.component.html',
    styleUrls: ['./availability-form.component.scss']
})
export class AvailabilityFormComponent implements OnInit, OnDestroy, HasToasty
{
    availability: IViewAvailabilityResource = <IViewAvailabilityResource>{
        id: 0,
        name: ''
    };

    subscription: Subscription;

    constructor(private _availabilitiesService: AvailabilitiesService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _toastyService: ToastyService) { }

    ngOnInit(): void
    {
        this._route.params.subscribe(p => this.availability.id = +p['id']);
        if (this.availability.id)
        {
            this.subscription = this._availabilitiesService
                .getAvailability(this.availability.id)
                .subscribe(a => this.availability = a);
        }
    }

    ngOnDestroy(): void
    {
        if (this.subscription) this.subscription.unsubscribe();
    }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Success',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 5000
        });
    }

    onToastyError(): void
    {
        this._toastyService.error({
            title: 'Error',
            msg: 'An unexpected error happened!',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
    }


    save(): void
    {
        let task$ = this.availability.id
            ? this._availabilitiesService.update(this.availability.id, this.availability)
            : this._availabilitiesService.create(this.availability);

        task$.subscribe(
            result =>
            {
                this.onToastySuccess(result.name + ' is saved!');
                this._router.navigate(['/admin/availabilities']);
            },
            err => this.onToastyError());
    }
}
