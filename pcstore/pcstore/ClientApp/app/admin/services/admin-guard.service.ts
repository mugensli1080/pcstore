import { Injectable, state } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { Auth0Service } from '../../shared/services/auth0/auth0.service';

@Injectable()
export class AdminGuardService implements CanActivate
{
    constructor(private _auth: Auth0Service) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>
    {
        if (this._auth.isAuthenticated() && this._auth.isRole('Admin') || this._auth.isRole('Moderator')) return true;

        this._auth.login();
        return false;
    }
}