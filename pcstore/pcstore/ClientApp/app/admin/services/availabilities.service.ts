import { IViewAvailabilityResource } from '../../shared/models/view/view-availability-resource';
import { Observable } from 'rxjs/Rx';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ISaveAvailabilityResource } from '../../shared/models/save/save-availability-resource';

@Injectable()
export class AvailabilitiesService
{
    private _base = '/api/availabilities';

    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getAvailabilities(): Observable<IViewAvailabilityResource[]>
    {
        return this._authHttp
            .get(this._base)
            .map(res => res.json());
    }

    getAvailability(id: number): Observable<IViewAvailabilityResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(availability: ISaveAvailabilityResource): Observable<IViewAvailabilityResource>
    {
        return this._authHttp
            .post(this._base, availability)
            .map(res => res.json());
    }

    update(id: number, availability: ISaveAvailabilityResource): Observable<IViewAvailabilityResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, availability)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}