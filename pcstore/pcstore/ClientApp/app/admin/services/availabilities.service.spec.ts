/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AvailabilitiesService } from './availabilities.service';

describe('Service: Availabilities', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AvailabilitiesService]
    });
  });

  it('should ...', inject([AvailabilitiesService], (service: AvailabilitiesService) => {
    expect(service).toBeTruthy();
  }));
});