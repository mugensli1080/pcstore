import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuardService } from '../shared/services/auth-guard.service';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { BreadcrumbService } from './services/breadcrumb.service';
import { HeaderComponent } from './components/header/header.component';
import { AppFooterComponent } from './components/app-footer/app-footer.component';
import { IBreadcrumb } from './models/breadcrumb/breadcrumb';
import { DirectoryType } from './models/breadcrumb/directory-type';
import { NAVIGATION_INFO } from './models/breadcrumb/navigation-directories';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { PromotionsComponent } from './components/promotions/promotions.component';

@NgModule({
    declarations: [
        HomeComponent,
        LoginComponent,
        NavMenuComponent,
        UserProfileComponent,
        BreadcrumbComponent,
        HeaderComponent,
        AppFooterComponent,
        AboutComponent,
        ContactComponent,
        PromotionsComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'login', component: LoginComponent },
            { path: 'home', component: HomeComponent },
            { path: 'user-profile', component: UserProfileComponent, canActivate: [AuthGuardService] }
        ])
    ],
    exports: [
        HomeComponent,
        LoginComponent,
        NavMenuComponent,
        UserProfileComponent,
        BreadcrumbComponent,
        HeaderComponent,
        AppFooterComponent,
        AboutComponent,
        ContactComponent,
        PromotionsComponent
    ],
    providers: [
        AuthGuardService,
        BreadcrumbService
    ]
})
export class CoreModule { }
