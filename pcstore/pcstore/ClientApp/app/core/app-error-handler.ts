import { ErrorHandler, Inject, isDevMode, NgZone } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import * as Raven from 'raven-js';

export class AppErrorHandler implements ErrorHandler
{
    constructor( @Inject(NgZone) private _zone: NgZone, @Inject(ToastyService) private _toastyService: ToastyService) { }

    handleError(error: any): void
    {
        if (!isDevMode) Raven.captureException(error.originalError || error);
        else throw error;

        this._zone.run(() =>
        {
            this._toastyService.error({
                title: 'Error',
                msg: 'An unexpected error happened!',
                theme: 'bootstrap',
                showClose: true,
                timeout: 5000
            });
        });
    }

}

