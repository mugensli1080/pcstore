import { NAVIGATION_INFO } from './navigation-directories';
export interface NavigationProperty
{
    name: string;
    url: string;
    parents: NAVIGATION_INFO[] | null;
}