import { NavigationProperty } from "./navigation-property";


export class NAVIGATION_INFO
{
    public static DIR_HOME = <NavigationProperty>{ name: 'Home', url: 'home' };

    public static DIR_PRODUCTS = <NavigationProperty>{ name: 'Products', url: 'products', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_MY_ORDERS = <NavigationProperty>{ name: 'My Orders', url: 'my/orders', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_SHOPPING_CART = <NavigationProperty>{ name: 'Shopping Cart', url: 'shopping-cart', parents: [NAVIGATION_INFO.DIR_HOME] };

    public static DIR_PRODUCTS_LIST = <NavigationProperty>{ name: 'Products List', url: 'admin/products', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_ORDERS = <NavigationProperty>{ name: 'Orders', url: 'admin/orders', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_CATEGORIES = <NavigationProperty>{ name: 'Categories', url: 'admin/categories', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_AVAILABILITIES = <NavigationProperty>{ name: 'Availabilities', url: 'admin/availabilities', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_PAYMENT_METHODS = <NavigationProperty>{ name: 'Payment Methods', url: 'admin/payment-methods', parents: [NAVIGATION_INFO.DIR_HOME] };
    public static DIR_SHIPPING_COMPANIES = <NavigationProperty>{ name: 'Shipping Companies', url: 'admin/shipping-companies', parents: [NAVIGATION_INFO.DIR_HOME] };
}

