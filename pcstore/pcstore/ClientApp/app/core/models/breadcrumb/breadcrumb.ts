import { NAVIGATION_INFO } from "./navigation-directories";
import { DirectoryType } from "./directory-type";

export interface IBreadcrumb
{
    type: DirectoryType;
    name?: string;
    url?: string;
    info: NAVIGATION_INFO;
}