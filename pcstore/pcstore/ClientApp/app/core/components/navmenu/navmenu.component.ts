import { Component, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss']
})
export class NavMenuComponent
{
    @ViewChild('chkMenu') chkMenu: ElementRef;
    @ViewChild('menu') menu: ElementRef;
    isOpenMenu = false;
    isAdmin: boolean = false;
    itemsCount$: Observable<number>;

    navItems = [
        { routerLink: "/products", name: "Products" },
        { routerLink: "", name: "About", id: "about" },
        { routerLink: "", name: "Careers" },
        { routerLink: "", name: "Contact Us" },
        { routerLink: "", name: "Terms" },
    ];

    menuItems = <any[]>[];

    userItems = [
        { routerLink: "/my/orders", name: "My Orders" }
    ];

    adminItems = [
        { routerLink: "/admin/products", name: "Manage Products" },
        { routerLink: "/admin/orders", name: "Manage Orders" },
        { routerLink: "/admin/payment-methods", name: "Manage Payment Methods" },
        { routerLink: "/admin/shipping-companies", name: "Manage Shipping Companies" },
        { routerLink: "/admin/categories", name: "Manage Categories" },
        { routerLink: "/admin/availabilities", name: "Manage Availabities Categories" },
    ];
    constructor(public auth0: Auth0Service, private _dexieDbService: DexieDbService) { }

    ngOnInit(): void
    {
        this.isAdmin = this.auth0.isRole('Admin');
        this.itemsCount$ = this._dexieDbService.trackCartTotal();
        if (this.isAdmin)
            this.menuItems = this.adminItems;
        else
            this.menuItems = this.userItems;
    }

    onMenuCheck(isOpen?: boolean)
    {
        this.isOpenMenu = isOpen ? false : !this.isOpenMenu;
    }

    onAdminClick()
    {
        this.isAdmin = !this.isAdmin;
        if (this.isAdmin) this.auth0.roles = ["Admin", "Moderator"];
        else this.auth0.roles = [];
    }

    onGoTo(id: string)
    {

    }

    onLogin()
    {
        this.menuItems = [];
        this.auth0.login();

    }

    onLogout()
    {
        this.auth0.logout();
        let chk = <HTMLInputElement>this.chkMenu.nativeElement;
        chk.checked = false;
        this.menuItems = [];
    }
}
