import { IViewProductResource } from '../../../shared/models/view/view-product-resource';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductService } from '../../../shared/services/product.service';
import { Subscription } from 'rxjs/Subscription';
import { UploadFolderPath } from '../../../shared/config/path';


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy
{

    images = [
        'https://cdn.wccftech.com/wp-content/uploads/2016/05/Nvidia-GTX-1080-Ti-Featured.jpg',
        'https://images.idgesg.net/images/article/2017/11/geforce-gtx-1070-ti-8-100740838-large.jpg',
        'https://d1urewwzb2qwii.cloudfront.net/sys-master/root/h6e/ha1/8885527052318/83da4f531786d98fc940d41c9f0c505d-razer-blade-pro-gallery-07.jpg'
    ];

    newProducts: IViewProductResource[];
    subscription: Subscription;
    path = UploadFolderPath.ProductPhotos;
    constructor(private _productService: ProductService) { }

    ngOnInit(): void
    {
        this.subscription = this._productService
            .getNewProducts()
            .subscribe(products => this.newProducts = products);
    }

    ngOnDestroy(): void
    {
        if (this.subscription) this.subscription.unsubscribe();
    }
}