import { Component } from '@angular/core';
import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { DexieDbService } from '../../../shared/services/dexiedb/dexiedb.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent
{

    constructor(public auth: Auth0Service,
        private _shoppingCartService: DexieDbService,
        private _dexieDbService: DexieDbService,
    )
    {
        auth.handleAuthentication();
    }

    async ngOnInit(): Promise<void>
    {
        let totalQuantity = await this._dexieDbService.totalCartQuanity();
        this._shoppingCartService.notify(totalQuantity);
    }
}
