import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { fade } from '../../../shared/ng-animations/ng-animations';
import { Utilities } from '../../../shared/ultilities/utilities';

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    animations: [fade]
})
export class HeaderComponent implements OnInit
{
    @ViewChild('headerSection') _headerSection: ElementRef;
    _opacity = 1;
    isScrolling = false;
    constructor( @Inject(DOCUMENT) private _document: Document) { }

    ngOnInit()
    {
        // this._document.documentElement.scrollTop = 0;
    }

    @HostListener('window:scroll', []) onWindowScroll()
    {
        const scrolling = this._document.documentElement.scrollTop;
        let headerSection = this._headerSection.nativeElement;
        this._opacity = (headerSection.offsetHeight - scrolling) / headerSection.offsetHeight;

        this.isScrolling = scrolling > 20 ? true : false;

    }

    toTop()
    {
        Utilities.toTop(this._document);
    }
}
