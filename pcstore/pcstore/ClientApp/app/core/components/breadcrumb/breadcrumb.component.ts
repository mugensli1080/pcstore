import { IBreadcrumb } from '../../models/breadcrumb/breadcrumb';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
    selector: 'breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy
{
    breadcrumb: IBreadcrumb;

    constructor(private _breadcrumbService: BreadcrumbService) { }

    ngOnInit()
    {
        this._breadcrumbService.trackBreadcrumb().subscribe(br => this.breadcrumb = br);
    }

    ngOnDestroy() { this._breadcrumbService.endTracking(); }

}
