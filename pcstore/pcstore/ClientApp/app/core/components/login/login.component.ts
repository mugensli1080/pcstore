import { Router } from '@angular/router';
import { Auth0Service } from '../../../shared/services/auth0/auth0.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent
{
    constructor(public _auth: Auth0Service, private _router: Router) { }
}
