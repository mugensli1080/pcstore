import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from '../../../shared/services/product.service';
import { IViewProductResource } from '../../../shared/models/view/view-product-resource';
import { Subscription } from 'rxjs';
import { asEnumerable } from 'linq-es5';
import { IViewProductPhotoResource } from '../../../shared/models/view/view-product-photo-resource';

@Component({
    selector: 'promotions',
    templateUrl: './promotions.component.html',
    styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit, OnDestroy
{
    products: IViewProductResource[] = [];
    promoProducts: IViewProductResource[] = [];
    productSubscription: Subscription;
    constructor(private _productService: ProductService) { }

    ngOnInit()
    {
        this.productSubscription = this._productService
            .getProducts()
            .subscribe(products =>
            {
                this.products = products;
                this.promoProducts = asEnumerable(products).Take(6).ToArray();
            });
    }

    ngOnDestroy()
    {
        if (this.productSubscription) this.productSubscription.unsubscribe();
    }

    getActivatedPhoto(photos: IViewProductPhotoResource[])
    {
        return photos.filter(x => x.activated == true) [0];
    }
}
