import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import { IBreadcrumb } from '../models/breadcrumb/breadcrumb';
import { DirectoryType } from '../models/breadcrumb/directory-type';


@Injectable()
export class BreadcrumbService
{
    private breadcrumb: Subject<IBreadcrumb>;
    private readonly BREADCRUMB_DATA_KEY = "breadcrumb";

    constructor(
        private _route: ActivatedRoute,
        private _router: Router)
    {
        this.setBreadcrumbs();
    }

    trackBreadcrumb()
    {
        this.breadcrumb = new Subject();
        return this.breadcrumb;
    }

    private notify(breadcrumb: IBreadcrumb)
    {
        if (this.breadcrumb) this.breadcrumb.next(breadcrumb);
    }

    endTracking()
    {
        if (this.breadcrumb) this.breadcrumb.complete();
    }

    private setBreadcrumbs()
    {
        this._router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe((event) =>
            {
                let navigationEnd = <NavigationEnd>event;
                this._route.children.forEach(activatedRoute =>
                {
                    let breadcrumb = <IBreadcrumb>activatedRoute.snapshot.data[this.BREADCRUMB_DATA_KEY];
                    if (breadcrumb && breadcrumb.type === DirectoryType.child)
                        this._route.queryParams.subscribe(p => breadcrumb.name = p['name']);

                    this.notify(breadcrumb);
                });
            });
    }
}