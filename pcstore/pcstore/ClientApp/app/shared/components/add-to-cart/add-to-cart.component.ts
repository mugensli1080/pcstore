import { Subscription } from 'rxjs/Rx';
import { ProductService } from '../../services/product.service';
import { IViewProductResource } from '../../models/view/view-product-resource';
import { IProductCard } from '../../models/product-card';
import { DexieDbService } from '../../services/dexiedb/dexiedb.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DexieCartItem } from '../../models/dexie-cart-item';
import { UploadFolderPath } from '../../config/path';
import { HasToasty } from '../../models/has-toasty';
import { ToastyService } from 'ng2-toasty';

@Component({
    selector: 'add-to-cart',
    templateUrl: './add-to-cart.component.html',
    styleUrls: ['./add-to-cart.component.scss']
})
export class AddToCartComponent implements HasToasty
{
    @Input('product-card') product: IProductCard;
    constructor(private _dexieDbService: DexieDbService,
        private _productService: ProductService,
        private _toastyService: ToastyService) { }

    onToastySuccess(msg: string): void
    {
        this._toastyService.success({
            title: 'Cheers',
            msg: msg,
            theme: 'bootstrap',
            showClose: false,
            timeout: 2000
        });
    }

    onToastyError(): void
    {
    }

    async addToCart(): Promise<void>
    {
        let item = await this._dexieDbService.getCartItem({ productId: this.product.id });
        let success = item
            ? this.updateItemDexieDb(item)
            : this.addItemToDexieDb();
    }

    private async addItemToDexieDb(): Promise<DexieCartItem | undefined>
    {
        let product = await this._productService.getProduct(this.product.id).toPromise();
        let cartItem = await this._dexieDbService.productToDexieCartItem(product, '/product/view/');

        this.onToastySuccess(product.name + ' is added to cart!');
        return await this._dexieDbService.addCartItem(cartItem);
    }

    private async updateItemDexieDb(item: DexieCartItem): Promise<DexieCartItem | undefined>
    {
        item.quantity++;
        this.onToastySuccess(item.productName + ' is added to cart!');
        return await this._dexieDbService.updateCartItem(item);
    }
}
