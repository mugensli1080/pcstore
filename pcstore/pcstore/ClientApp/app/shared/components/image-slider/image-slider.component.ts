import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'image-slider',
    templateUrl: './image-slider.component.html',
    styleUrls: ['./image-slider.component.scss'],
    animations: [
        trigger(
            'slide', [
                transition(':enter', [
                    style({ transform: 'translateX(100%)', opacity: 0 }),
                    animate('2000ms', style({ transform: 'translateX(0%)', opacity: 1 }))
                ]),
                transition(':leave', [
                    style({ transform: 'translateX(0%)', opacity: 1 }),
                    animate('2000ms', style({ transform: 'translateX(-100%)', opacity: 0 })
                    )])
            ]
        )
    ]
})
export class ImageSliderComponent implements OnInit
{
    @Input('images') images: any[];

    currentImage = <any[]>[];
    constructor() { }

    ngOnInit()
    {
        this.currentImage = [];
        let headerBackground = {
            index: 0,
            image: this.images[0]
        };

        this.currentImage.push(headerBackground);
        // this.startSlider();
    }

    private startSlider()
    {
        setInterval(() =>
        {
            let currentImagge = this.currentImage[0];
            if (currentImagge.index < this.images.length - 1) currentImagge.index += 1;
            else currentImagge.index = 0;

            this.currentImage.splice(0, 1);

            this.currentImage.push({
                index: currentImagge.index,
                image: this.images[currentImagge.index]
            });
        }, 5000);
    }

}
