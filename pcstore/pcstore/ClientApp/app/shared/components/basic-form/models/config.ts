export interface BFFormSettings
{
    validation: boolean;
    title?: string;
}

export interface BFInputConfig
{
    field: string;
    isRequired?: boolean;
    isReadonly?: boolean;
    isUrl?: boolean;
    min?: number;
    max?: number;
    label: string;
    type: BFTypes;
    array?: BFKeyValuePair[];
}

export interface BFKeyValuePair
{
    key: number;
    value: string;
}

export enum BFTypes
{
    text = 1,
    number = 2,
    select = 3,
    textarea = 4,
    radio = 5,
    checkbox = 6
}