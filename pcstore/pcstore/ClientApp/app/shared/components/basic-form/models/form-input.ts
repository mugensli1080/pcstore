
export interface BasicFormInput
{
    label: { activated: boolean, value: string };
    id: string;
    name: string;
    input: BasicFormInputAttr;
    isRequired: boolean;
}

export interface BasicFormInputAttr
{
    activated: boolean;
    value: any;
    type: string;
    isInputField?: boolean;
    isSelect?: boolean;
    isTextArea?: boolean;
}

