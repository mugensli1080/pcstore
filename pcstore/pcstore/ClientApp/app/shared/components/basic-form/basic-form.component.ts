
import { BFInputConfig, BFFormSettings } from './models/config';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'basic-form',
    templateUrl: './basic-form.component.html',
    styleUrls: ['./basic-form.component.scss']
})
export class BasicFormComponent implements OnInit
{
    @Input('form-inputs') formObject: any;
    @Input('form-settings') formSettings: BFFormSettings;
    @Input('input-config') inputConfigs: BFInputConfig[];
    @Output('form-change') formChange: EventEmitter<any> = new EventEmitter();


    keys: string[];
    basicForm: any;

    constructor(private renderer: Renderer) { }

    ngOnInit(): void
    {
        this.keys = Object.keys(this.formObject);
    }

    save(form: any)
    {
        console.log("saving");
        this.formChange.emit(form);
    }
}
