import { Component, Input } from '@angular/core';

import { IProductCard } from '../../models/product-card';

@Component({
    selector: 'product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent
{
    @Input('product-card') productCard: IProductCard;
    pictureNumber = Math.floor((Math.random() * 7) + 1);
}
