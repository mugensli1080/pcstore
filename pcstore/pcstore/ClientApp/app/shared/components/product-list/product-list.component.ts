import { Component, Input, OnInit } from '@angular/core';
import { IViewProductResource } from '../../models/view/view-product-resource';
import { IProductCard } from '../../models/product-card';
import { ProductService } from '../../services/product.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit
{
    @Input('product') product: IViewProductResource;
    @Input('image') image: string;
    productCard: IProductCard;

    constructor(private _productService: ProductService) { }

    ngOnInit()
    {
        this.productCard = this._productService.createProductCard(this.product);
    }
}
