import { Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';

import { IResponsiveTableOptions } from '../../models/rt-option';
import { Utilities } from '../../ultilities/utilities';

@Component({
    selector: 'responsive-table',
    templateUrl: './responsive-table.component.html',
    styleUrls: ['./responsive-table.component.scss']
})
export class ResponsiveTableComponent implements OnInit
{
    @Input('rtoption') option: IResponsiveTableOptions;
    @Output('remove-item') removeItem: EventEmitter<any> = new EventEmitter();
    @Output('emit-value') eValue: EventEmitter<any> = new EventEmitter();

    private isSortAscending: boolean = true;

    pageSize = 10;
    itemsList: any[] = [];
    currentPage = 1;
    pages: number[] = [];
    searchParam: string;
    thumbnailWidth = '10%';
    hasHeaderColumns: boolean;

    constructor(private _zone: NgZone)
    {
        this.option = <IResponsiveTableOptions>{};
    }

    ngOnInit(): void
    {
        let screenWidth = window.innerWidth;
        console.log("screen width: ", screenWidth);
        let width = window.screen.width > 600 ? '10%' : '100%';
        this.thumbnailWidth = this.option.thumbnailWidth ? this.option.thumbnailWidth : width;
        this.hasHeaderColumns = this.option.hasHeaderColumns == undefined ? true : this.option.hasHeaderColumns;
        this.itemsList = this.paginate(this.option.items!);
        this.sortBy('');
    }

    emitValue(item: any, columnName: any, value: any): void
    {
        item[columnName] = value;
        this.eValue.emit(item);
    }

    private paginate(arr: any[]): object[]
    {
        return Utilities.paginate(arr, this.pageSize);
    }

    onRemoveItem(id: number): void
    {
        this.removeItem.emit(id);
        let newItems = this.option.items!.filter(i => i.id !== id);
        this.itemsList = this.paginate(newItems);
    }

    get totalItems(): number
    {
        let total = 0;
        this.pages = [];
        for (let i = 0; i < this.itemsList.length; i++)
        {
            total += this.itemsList[i].length;
            this.pages.push(i + 1);
        }

        return total;
    }

    onSearch(): void
    {
        let foundList = Utilities.searchString(this.option.items!, 'name', this.searchParam);
        this.itemsList = this.paginate(foundList!);
    }

    sortBy(column: string): void
    {
        let list = Utilities.sortColumn(this.option.items!, column, this.isSortAscending);
        this.itemsList = this.paginate(list);
        this.isSortAscending = !this.isSortAscending;
    }

    previous(): void
    {
        if (this.currentPage == 1) return;
        this.currentPage--;
    }

    next(): void
    {
        if (this.currentPage == this.pages.length) return;
        this.currentPage++;
    }

    changePage(page: number): void { this.currentPage = page; }
}
