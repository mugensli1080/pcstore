import { PhotoType } from './photo.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';

import { ISaveProductResource } from '../models/save/save-product-resource';
import { IProductCard } from '../models/product-card';
import { UploadFolderPath } from '../config/path';
import { IViewProductResource } from '../models/view/view-product-resource';

@Injectable()
export class ProductService
{

    private _base = '/api/products';
    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getProducts(): Observable<IViewProductResource[]>
    {
        return this._http
            .get(this._base)
            .map(res => res.json());
    }

    getNewProducts(): Observable<IViewProductResource[]>
    {
        return this._http
            .get(this._base + '/newproducts')
            .map(res => res.json());
    }

    getProduct(id: number): Observable<IViewProductResource>
    {
        return this._http
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(product: ISaveProductResource): Observable<IViewProductResource>
    {
        return this._authHttp
            .post(this._base, product)
            .map(res => res.json());
    }

    update(id: number, product: ISaveProductResource): Observable<IViewProductResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, product)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }

    createProductCard(product: IViewProductResource): IProductCard
    {
        let image = product.productPhotos.filter(p => p.activated === true)[0]
            ? product.productPhotos.filter(p => p.activated === true)[0].thumbnail
            : '';
        return <IProductCard>{
            id: product.id,
            title: product.name,
            descripton: product.description,
            price: product.price,
            imageUrl: UploadFolderPath.ProductPhotos + '' + image,
            link: '/product/view/' + product.id,
            inStock: product.stock > 0,
            availability: product.availabilityName
        };
    }
}