/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ShippingMethodService } from './shipping-method.service';

describe('Service: ShippingMethod', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShippingMethodService]
    });
  });

  it('should ...', inject([ShippingMethodService], (service: ShippingMethodService) => {
    expect(service).toBeTruthy();
  }));
});