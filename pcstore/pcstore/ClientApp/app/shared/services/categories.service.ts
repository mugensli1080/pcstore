
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { ICategoryResource } from '../models/category-resource';

@Injectable()
export class CategoriesService
{
    private _base = "/api/categories";
    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getCategories(): Observable<ICategoryResource[]>
    {
        return this._http
            .get(this._base)
            .map(res => res.json());
    }

    getCategory(id: number): Observable<ICategoryResource>
    {
        return this._http
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(category: ICategoryResource): Observable<ICategoryResource>
    {
        return this._authHttp
            .post(this._base, category)
            .map(res => res.json());
    }

    update(id: number, category: ICategoryResource): Observable<ICategoryResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, category)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}