import { IViewProductResource } from '../../models/view/view-product-resource';
import { Injectable, OnDestroy, OnInit } from '@angular/core';
import Dexie from 'dexie';
import { Subject } from 'rxjs/Rx';

import { DexieCartItem } from '../../models/dexie-cart-item';
import { ProductService } from '../product.service';
import { UploadFolderPath } from '../../config/path';
import { IViewCartItemResource } from '../../models/view/view-cart-item-resource';

@Injectable()
export class DexieDbService extends Dexie implements OnInit, OnDestroy
{
    private cartItems: Dexie.Table<DexieCartItem, number>;

    shoppingCartCount: Subject<number>;

    constructor(private _productService: ProductService)
    {
        super("CartDB");
        let cart = <DexieCartItem>{
            id: 0,
            productId: 0,
            productName: '',
            unitPrice: 0,
            quantity: 0,
            availability: '',
            thumbnail: '',
            url: '',
            created: new Date(),
            modified: new Date()
        };
        let keys = Object.keys(cart).toString();
        this.version(1).stores({ cartItems: "++" + keys });
        this.cartItems.mapToClass(DexieCartItem);
    }

    async ngOnInit(): Promise<void>
    {
        await this.syncDb();
        this.trackCartTotal();
        this.notify(await this.totalCartQuanity());
    }

    ngOnDestroy(): void
    {
        this.endTrackingCart();
    }

    trackCartTotal(): Subject<number>
    {
        this.shoppingCartCount = new Subject();
        return this.shoppingCartCount;
    }

    notify(count: number): void
    {
        this.shoppingCartCount.next(count);
    }

    endTrackingCart(): void
    {
        if (this.shoppingCartCount) this.shoppingCartCount.complete();
    }

    async removeAllItems(): Promise<void>
    {
        this.cartItems.clear();
        this.notify(await this.totalCartQuanity());
    }

    async getAllCartItems(): Promise<DexieCartItem[]>
    {
        return await this.cartItems.toArray();
    }

    async getCartItem(key: any): Promise<DexieCartItem | undefined>
    {
        return await this.cartItems.get(key);
    }

    async addCartItem(item: DexieCartItem): Promise<DexieCartItem | undefined>
    {
        let id = await this.cartItems.add(item);
        this.notify(await this.totalCartQuanity());
        return await this.getCartItem({ id: id });
    }

    async updateCartItem(item: DexieCartItem): Promise<DexieCartItem | undefined>
    {
        let id = await this.cartItems.put(item);
        this.notify(await this.totalCartQuanity());
        return await this.getCartItem({ id: id });
    }

    async deleteCartItem(key: any): Promise<void>
    {
        await this.cartItems.delete(key);
        this.notify(await this.totalCartQuanity());
    }

    async totalCartQuanity(): Promise<number>
    {
        let total = 0;
        let items = await this.cartItems.toArray();
        items.forEach(item => { total += parseInt(item.quantity + ""); });
        return total;
    }

    productToDexieCartItem(product: IViewProductResource, url: string): DexieCartItem
    {
        return <DexieCartItem>{
            productId: product.id,
            productName: product.name,
            unitPrice: product.price,
            quantity: 1,
            availability: product.availabilityName,
            thumbnail: UploadFolderPath.ProductPhotos + product.productPhotos.filter(p => p.activated === true)[0].name,
            url: url + product.id,
            created: product.created,
            modified: product.modified
        };
    }

    async syncDb()
    {
        let items = await this.getAllCartItems();
        items.forEach(item => { this.updateCartItem(item); });
    }

    // private async syncProduct(dexieCartItem: DexieCartItem)
    // {
    //     let product = await this._productService.getProductUser(dexieCartItem.productId).toPromise();
    //     dexieCartItem.productName = product.name;
    //     dexieCartItem.availability = product.availability;
    //     dexieCartItem.thumbnail = product.photo;
    //     dexieCartItem.price = product.price;
    //     dexieCartItem.quantity = parseInt(dexieCartItem.quantity + '');
    //     return dexieCartItem;
    // }
}