/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DexieDbService } from './dexiedb.service';

describe('Service: ShoppingCart', () =>
{
    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [DexieDbService]
        });
    });

    it('should ...', inject([DexieDbService], (service: DexieDbService) =>
    {
        expect(service).toBeTruthy();
    }));
});