import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';

import { ISaveShippingCompanyResource } from '../models/save/save-shipping-company-resource';
import { ISaveShippingMethodResource } from '../models/save/save-shipping-method-resource';
import { IViewShippingCompanyResource } from '../models/view/view-shipping-company-resource';

@Injectable()
export class ShippingCompanyService
{
    private _base = '/api/shippingcompanies';
    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getShippingCompanies(): Observable<any[]>
    {
        return this._authHttp
            .get(this._base)
            .map(res => res.json());
    }

    getShippingCompany(id: number): Observable<IViewShippingCompanyResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json())
    }

    create(shippingCompany: ISaveShippingCompanyResource): Observable<IViewShippingCompanyResource>
    {
        return this._authHttp
            .post(this._base, shippingCompany)
            .map(res => res.json());
    }

    update(id: number, shippingCompany: ISaveShippingCompanyResource): Observable<IViewShippingCompanyResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, shippingCompany)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}