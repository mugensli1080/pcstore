import { ISubCategoryResource } from '../models/sub-categetory-resource';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs';

@Injectable()
export class SubCategoriesService
{
    private _base = "/api/subcategories";
    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getSubCategories(categoryId: number): Observable<ISubCategoryResource[]>
    {
        return this._http
            .get(this._base)
            .map(res => res.json());
    }

    getSubCategory(categoryId: number, id: number): Observable<ISubCategoryResource>
    {
        return this._http
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(categoryId: number, subCategory: any): Observable<ISubCategoryResource>
    {
        return this._authHttp
            .post(this._base, subCategory)
            .map(res => res.json());
    }

    update(categoryId: number, subCategory: any): Observable<ISubCategoryResource>
    {
        return this._authHttp
            .put(this._base + '/' + subCategory.id, subCategory)
            .map(res => res.json());
    }

    delete(categoryId: number, id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}