import { IViewSpecificationPhotoResource } from '../models/view/view-specification-photo-resource';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { IViewProductPhotoResource } from '../models/view/view-product-photo-resource';

export enum PhotoType
{
    Product = 1,
    Specification = 2
}

@Injectable()
export class PhotoService
{
    constructor(private _http: Http, private _authHttp: AuthHttp) { }

    getPhotos(productId: number): Observable<any>
    {
        return this._http
            .get(this._base(productId))
            .map(res => res.json());
    }

    delete(productId: number, id: number, photoType: PhotoType): Observable<any>
    {
        return this._authHttp
            .delete(this._base(productId, photoType) + '/' + id)
            .map(res => res.json());
    }

    activateProductPhoto(productId: number, id: number): Observable<IViewProductPhotoResource>
    {
        return this._authHttp
            .put(this._base(productId, PhotoType.Product) + '/' + id, null)
            .map(res => res.json());
    }

    upload(productId: number, photoType: PhotoType, photo: any): Observable<any>
    {
        let formData = new FormData();
        formData.append('file', photo);
        return this._authHttp
            .post(this._base(productId, photoType), formData)
            .map(res => res.json());
    }

    private _base(productId: number, photoType?: PhotoType): string
    {
        let type = '';
        switch (photoType)
        {
            case PhotoType.Product:
                type = '/product';
                break;
            case PhotoType.Specification:
                type = '/specification';
                break;
        }
        return '/api/products/' + productId + '/photos' + type;
    }
}