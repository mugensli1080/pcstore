/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ShippingCompanyService } from './shipping-company.service';

describe('Service: Shipping', () =>
{
    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [ShippingCompanyService]
        });
    });

    it('should ...', inject([ShippingCompanyService], (service: ShippingCompanyService) =>
    {
        expect(service).toBeTruthy();
    }));
});