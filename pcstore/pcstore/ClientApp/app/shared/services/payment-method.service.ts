import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { ISavePaymentMethodResource } from '../models/save/save-payment-resource';
import { IViewPaymentMethodResource } from '../models/view/view-payment-method-resource';

@Injectable()
export class PaymentMethodService
{
    private _base = '/api/paymentmethods';
    constructor(private _authHttp: AuthHttp) { }

    getPaymentMethods()
    {
        return this._authHttp
            .get(this._base)
            .map(res => res.json());
    }

    getPaymentMethod(id: number): Observable<IViewPaymentMethodResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(paymentMethod: ISavePaymentMethodResource): Observable<IViewPaymentMethodResource>
    {
        return this._authHttp
            .post(this._base, paymentMethod)
            .map(res => res.json());
    }

    update(id: number, paymentMethod: ISavePaymentMethodResource): Observable<IViewPaymentMethodResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, paymentMethod)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}