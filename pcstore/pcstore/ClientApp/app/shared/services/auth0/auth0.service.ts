import 'rxjs/add/operator/filter';

import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';
import * as auth0 from 'auth0-js';

import { AUTH_CONFIG } from './config/auth0-variable';

@Injectable()
export class Auth0Service
{
    auth0 = new auth0.WebAuth({
        clientID: AUTH_CONFIG.clientID,
        domain: AUTH_CONFIG.domain,
        responseType: 'token id_token',
        audience: AUTH_CONFIG.apiUrl,
        redirectUri: AUTH_CONFIG.callbackURL,
        scope: 'openid profile email'
    });

    userProfile: auth0.Auth0UserProfile;
    roles: string[] = [];
    constructor(public router: Router, private _location: Location)
    {
        this.userProfile = JSON.parse(localStorage.getItem('user-profile')!);
        this.setRole();
    }

    public login(): void
    {
        this.auth0.authorize();
    }

    public handleAuthentication(): void
    {
        this.auth0.parseHash((err, authResult) =>
        {
            if (authResult && authResult.accessToken && authResult.idToken)
            {
                window.location.hash = '';
                this.setSession(authResult);
                let currentLocation = localStorage.getItem('current-location');
                if (currentLocation) this.router.navigate([currentLocation]);
                else this.router.navigate(['/home']);
            } else if (err)
            {
                this.router.navigate(['/home']);
                console.log(err);
                alert(`Error: ${err.error}. Check the console for further details.`);
            }
        });
    }

    private setUserProfile(authResult: any): void
    {
        this.auth0.client.userInfo(authResult.accessToken, (err, user) =>
        {
            if (err) throw err;
            else
            {
                user.user_id = user.sub.split('|')[1];
                localStorage.setItem('user-profile', JSON.stringify(user));
                this.userProfile = user;
            }
        });
    }

    private setSession(authResult: any): void
    {
        // Set the time that the access token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 10000) + new Date().getTime());
        localStorage.setItem('token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);

        this.setRole();
        this.setUserProfile(authResult);
    }

    public logout(): void
    {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('token');    // require for authHttp
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('user-profile');
        // Go back to the home route
        this.router.navigate(['/']);
    }

    public isAuthenticated(): boolean
    {
        const expiresAt = JSON.parse(localStorage.getItem('expires_at')!);
        return new Date().getTime() < expiresAt;
    }

    public isRole(roleName: string)
    {
        return this.roles.indexOf(roleName) > -1;
    }


    private setRole(): void
    {
        let jwtHelper = new JwtHelper();
        let token = localStorage.getItem('token');
        if (token)
        {
            let decodedToken = jwtHelper.decodeToken(localStorage.getItem('token')!);
            this.roles = decodedToken['https://pc_store.com/roles'] || [];
        }
    }
}

