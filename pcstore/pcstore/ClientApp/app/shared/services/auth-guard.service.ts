import { Location } from '@angular/common';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Auth0Service } from './auth0/auth0.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate
{
    constructor(private _auth: Auth0Service, private _location: Location) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>
    {
        if (this._auth.isAuthenticated() && !this._auth.isRole('Admin') && !this._auth.isRole('Moderator')) return true;

        this._auth.login();
        return false;
    }
}