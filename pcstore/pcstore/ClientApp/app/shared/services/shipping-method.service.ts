import { ISaveShippingMethodResource } from '../models/save/save-shipping-method-resource';
import { ISaveShippingCompanyResource } from '../models/save/save-shipping-company-resource';
import { Observable } from 'rxjs/Rx';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { IViewShippingMethodResource } from '../models/view/view-shipping-method-resource';

@Injectable()
export class ShippingMethodService
{
    private _base = '/api/shippingmethods';
    constructor(private _authHttp: AuthHttp) { }

    getShippingMethods(): Observable<IViewShippingMethodResource[]>
    {
        return this._authHttp
            .get(this._base)
            .map(res => res.json());
    }

    getShippingMethod(id: number): Observable<IViewShippingMethodResource>
    {
        return this._authHttp
            .get(this._base + '/' + id)
            .map(res => res.json());
    }

    create(shippingMethod: ISaveShippingCompanyResource): Observable<IViewShippingMethodResource>
    {
        return this._authHttp
            .post(this._base, shippingMethod)
            .map(res => res.json());
    }

    update(id: number, shippingMethod: ISaveShippingMethodResource): Observable<IViewShippingMethodResource>
    {
        return this._authHttp
            .put(this._base + '/' + id, shippingMethod)
            .map(res => res.json());
    }

    delete(id: number): Observable<number>
    {
        return this._authHttp
            .delete(this._base + '/' + id)
            .map(res => res.json());
    }
}