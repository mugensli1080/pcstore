import { animate, animation, style, transition, trigger, useAnimation } from '@angular/animations';

export const fadeInAnimation = animation([
    style({ opacity: 0 }),
    animate(2000)
]);

export const fadeOutAnimation = animation([
    style({ opacity: 1 }),
    animate(2000)
]);

export let fade = trigger('fade', [
    transition(':enter', useAnimation(fadeInAnimation)),
    transition(':leave', useAnimation(fadeOutAnimation))
]);


export const fadeInAnimationFast = animation([
    style({ opacity: 0 }),
    animate(200)
]);

export const fadeOutAnimationFast = animation([
    style({ opacity: 1 }),
    animate(200)
]);

export let fadeFast = trigger('fadeFast', [
    transition(':enter', useAnimation(fadeInAnimationFast)),
    transition(':leave', useAnimation(fadeOutAnimationFast))
]);

