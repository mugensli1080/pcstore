import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AuthConfig, AuthHttp } from 'angular2-jwt';
import { ToastyComponent, ToastyModule } from 'ng2-toasty';
import { CustomFormsModule } from 'ng2-validation';

import { ProductFilterComponent } from '../client/components/products/product-filter/product-filter.component';
import { AddToCartComponent } from './components/add-to-cart/add-to-cart.component';
import { BasicFormComponent } from './components/basic-form/basic-form.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ResponsiveTableComponent } from './components/responsive-table/responsive-table.component';
import { Auth0Service } from './services/auth0/auth0.service';
import { CategoriesService } from './services/categories.service';
import { DexieDbService } from './services/dexiedb/dexiedb.service';
import { PaymentMethodService } from './services/payment-method.service';
import { PhotoService } from './services/photo.service';
import { ProductService } from './services/product.service';
import { ProgressService } from './services/progress.service';
import { ShippingCompanyService } from './services/shipping-company.service';
import { ShippingMethodService } from './services/shipping-method.service';
import { SubCategoriesService } from './services/sub-categories.service';
import { } from 'jasmine';
import { ImageSliderComponent } from './components/image-slider/image-slider.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { SafePipe } from './pipes/safe';
export function authHttpServiceFactory(http: Http, options: RequestOptions)
{
    return new AuthHttp(new AuthConfig({}), http, options);
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([]),
        CustomFormsModule,
        BrowserModule,
        ToastyModule.forRoot().ngModule,
        BrowserAnimationsModule
    ],
    declarations: [
        ProductCardComponent,
        ProductListComponent,
        ResponsiveTableComponent,
        ProductFilterComponent,
        AddToCartComponent,
        BasicFormComponent,
        ImageSliderComponent,
        SafePipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        HttpModule,
        ProductCardComponent,
        ProductListComponent,
        ResponsiveTableComponent,
        RouterModule,
        ProductFilterComponent,
        AddToCartComponent,
        BasicFormComponent,
        ToastyComponent,
        ImageSliderComponent,
        SafePipe
    ],
    providers: [
        CategoriesService,
        PhotoService,
        ProductService,
        ProgressService,
        SubCategoriesService,
        DexieDbService,
        //AUTH_PROVIDERS,
        { provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http, RequestOptions] },
        Auth0Service,
        DexieDbService,
        ShippingCompanyService,
        PaymentMethodService,
        ShippingMethodService,
    ]
})
export class SharedModule { }