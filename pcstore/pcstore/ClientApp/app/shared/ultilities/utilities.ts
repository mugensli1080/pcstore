import { asEnumerable } from "linq-es5";
import { Page } from "../models/page";

export class Utilities
{
    /**
     * 
     * @param document Html Document
     * 
     * Go to the top of the document
     */
    static toTop(document: Document)
    {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    /**
     * 
     * @param array: array of type <T>
     * 
     * Breaks an array into sub groups (pages)
     * returns Page<T> Array
     */
    static paginate<T>(array: T[], groupSize: number): Page<T>[]
    {
        return <Page<T>[]>asEnumerable(array)
            .Select((item, index) =>
            {
                return <Page<T>>{ page: Math.ceil((index! + 1) / groupSize), item: item };
            })
            .GroupBy(item => item.page)
            .ToArray();
    }

    /**
     * 
     * @param collection table records
     * @param column column to order
     * @param isAscending search order
     * 
     * Breaks an array into sub groups (pages)
     * returns Page<T> Array
     */
    static sortColumn(collection: any[], column: string, isAscending: boolean = false): any[]
    {
        return isAscending
            ? asEnumerable(collection).OrderByDescending(i => i[column]).ToArray()
            : asEnumerable(collection).OrderBy(i => i[column]).ToArray();
    }

    /**
     * 
     * @param collection records
     * @param property collection item's property
     * @param searchParam param to search
     * 
     * Breaks an array into sub groups (pages)
     * returns Page<T> Array
     */
    static searchString(collection: any[], property: string, searchParam: string)
    {
        return searchParam
            ? asEnumerable(collection)
                .Where(item => (<string>item[property])
                    .toLowerCase()
                    .includes(searchParam.toLowerCase()))
                .ToArray()
            : collection;
    }
}