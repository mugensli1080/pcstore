import { IViewProductResource } from "./view/view-product-resource";

export interface HasToasty
{
    onToastySuccess(msg: string): void;
    onToastyError(err?: any): void;
}