export interface Page<T>
{
    page: number;
    item: T;
}