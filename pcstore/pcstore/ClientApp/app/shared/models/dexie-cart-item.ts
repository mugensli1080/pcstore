export class DexieCartItem
{
    id: number;
    productId: number;
    productName: string;
    unitPrice: number;
    quantity: number;
    availability: string;
    thumbnail: string;
    url: string;
    created: Date;
    modified: Date;
    get subTotal() { return this.quantity * this.unitPrice; }
}