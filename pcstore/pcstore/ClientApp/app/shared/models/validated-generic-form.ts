export interface IValidatedGenericForm<T>
{
    isvalid: boolean;
    form: T;
}