
export interface IResponsiveTableOptions
{
    caption?: string;
    columns: IRTColumnOption[];
    items?: any[];
    hasThumbnail: boolean;
    hasHeaderColumns?: boolean;
    thumbnailWidth?: string;
    itemLinkUrl?: string;
    paging?: boolean;
    search?: boolean;
}

export interface IRTColumnOption
{
    name?: string;
    isAlignCenter?: boolean;
    isBold?: boolean;
    isCurrency?: boolean;
    isRemovableItem?: boolean;
    isLink?: boolean;
    isInputField?: {
        number: boolean,
        text: boolean
    }
    fieldWidth?: any;
    url?: string;
}