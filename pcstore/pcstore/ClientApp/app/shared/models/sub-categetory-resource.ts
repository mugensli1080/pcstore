export interface ISubCategoryResource
{
    id: number;
    name: string;
    categoryId: number;
}