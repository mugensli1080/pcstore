export interface IViewOrderItemResource
{
    id: number;
    productId: number;
    productName: string;
    unitPrice: number;
    quantity: number;
    thumbnail: string;
    subTotal: number;
}