export interface IViewPaymentMethodResource
{
    id: number;
    name: string;
    surcharge: number;
    important: string;
    note: string;
    created: Date;
    modified: Date;
}