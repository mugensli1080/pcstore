export interface IViewSpecificationPhotoResource
{
    id: number;
    name: string;
    thumbnail: string;
    index: number;
    productId: number;
    created: Date;
    modified: Date;
}