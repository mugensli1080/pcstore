import { IViewCartItemResource } from './view-cart-item-resource';

export interface IViewShoppingCartResource
{
    id: number;
    reference: string;
    auth0Id: string;
    email: string;
    cartItems: IViewCartItemResource[];
    totalPrice: number;
    created: Date;
    modified: Date;
}