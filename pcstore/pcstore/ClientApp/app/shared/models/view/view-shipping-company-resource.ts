import { IViewProductPhotoResource } from "./view-product-photo-resource";
import { IViewShippingMethodResource } from "./view-shipping-method-resource";

export interface IViewShippingCompanyResource
{
    id: number;
    productPhoto: IViewProductPhotoResource;
    company: string;
    shippingMethods: IViewShippingMethodResource[];
    slogan: string;
    specialNote: string;
    created: Date;
    modified: Date;
}