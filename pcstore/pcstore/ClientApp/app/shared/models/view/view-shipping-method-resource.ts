export interface IViewShippingMethodResource
{
    id: number;
    deliveryMethod: string;
    price: number;
    estimateTime: string;
    shippingCompanyId: number;
    created: Date;
    modified: Date;
}