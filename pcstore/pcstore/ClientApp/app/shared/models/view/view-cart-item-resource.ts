export interface IViewCartItemResource
{
    id: number;
    productId: number;
    productName: string;
    unitPrice: number;
    quantity: number;
    availability: string;
    thumbnail: string;
    subTotal: number;
    created: Date;
    modified: Date;
}