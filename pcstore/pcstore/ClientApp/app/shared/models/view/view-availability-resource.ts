export interface IViewAvailabilityResource
{
    id: number;
    name: string;
    created: Date;
    modified: Date;
}