export interface IViewAddressResource
{
    id: number;
    firstName: string;
    lastName: string;
    company: string;
    street: string;
    streetCont?: string;
    postCode: string;
    suburb: string;
    state: string;
    country: string;
    type: string;
    created: Date;
    modified: Date;
}