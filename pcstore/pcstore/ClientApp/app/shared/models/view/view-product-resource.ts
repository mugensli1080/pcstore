import { IViewProductPhotoResource } from './view-product-photo-resource';
import { IViewSpecificationPhotoResource } from './view-specification-photo-resource';

export interface IViewProductResource
{
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    availabilityId: number;
    availabilityName: string;
    categoryId: number;
    categoryName: string;
    subCategoryId: number;
    subCategoryName: string;
    productPhotos: IViewProductPhotoResource[];
    specificationPhotos: IViewSpecificationPhotoResource[];
    videoLink: string;
    created: Date;
    modified: Date;
}