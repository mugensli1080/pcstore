import { IViewAddressResource } from "./view-address-resource";
import { IViewShippingMethodResource } from "./view-shipping-method-resource";
import { IViewPaymentMethodResource } from "./view-payment-method-resource";
import { IViewOrderItemResource } from "./view-order-item-resource";

export interface IViewOrderResource
{
    id: number;
    reference: string;
    auth0Id: string;
    email: string;
    orderItems: IViewOrderItemResource[];
    addresses: IViewAddressResource[];
    shippingMethod: IViewShippingMethodResource;
    paymentMethod: IViewPaymentMethodResource;
    created: Date;
    modified: Date;
}