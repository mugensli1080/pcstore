export interface IViewProductPhotoResource
{
    id: number;
    name: string;
    thumbnail: string;
    productId: number;
    activated: boolean;
    created: Date;
    modified: Date;
}