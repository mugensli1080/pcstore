import { ISaveCartItemResource } from './save-cart-item-resource';
export interface ISaveShoppingCartResource
{
    email: string;
    auth0Id: string;
    cartItems: ISaveCartItemResource[];
}