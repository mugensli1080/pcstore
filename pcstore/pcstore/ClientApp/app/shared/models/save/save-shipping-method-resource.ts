export interface ISaveShippingMethodResource
{
    deliveryMethod: string;
    price: number;
    estimateTime: string;
}