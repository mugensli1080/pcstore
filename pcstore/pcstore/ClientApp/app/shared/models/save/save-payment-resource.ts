export interface ISavePaymentMethodResource
{
    name: string;
    surcharge: number;
    important?: string;
    note?: string;
}