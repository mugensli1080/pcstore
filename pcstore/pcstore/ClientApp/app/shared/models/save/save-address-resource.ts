export interface ISaveAddressResource
{
    firstName: string;
    lastName: string;
    company: string;
    street: string;
    streetCont?: string;
    postCode: string;
    suburb: string;
    state: string;
    country: string;
    type: string;
}