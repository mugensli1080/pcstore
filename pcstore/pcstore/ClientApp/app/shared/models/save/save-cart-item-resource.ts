export interface ISaveCartItemResource
{
    productId: number;
    quantity: number;
}