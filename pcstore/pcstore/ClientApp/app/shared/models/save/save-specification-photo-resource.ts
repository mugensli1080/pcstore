export interface ISaveSpecificationPhotoResource
{
    name: string;
    thumbnail: string;
    productId: number;
    index: number;
}