export interface ISaveProductPhotoResource
{
    name: string;
    thumbnail: string;
    productId: number;
    activated: boolean;
}