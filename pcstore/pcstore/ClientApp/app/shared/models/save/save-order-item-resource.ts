export interface ISaveOrderItemResource
{
    productId: number;
    quantity: number;
}