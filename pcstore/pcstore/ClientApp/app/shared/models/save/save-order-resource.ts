import { ISaveOrderItemResource } from './save-order-item-resource';
import { ISaveAddressResource } from "./save-address-resource";

export interface ISaveOrderResource
{
    auth0Id: string;
    email: string;
    reference: string;
    orderItems: ISaveOrderItemResource[];
    addresses: ISaveAddressResource[];
    shippingMethodId: number;
    paymentMethodId: number;
}
