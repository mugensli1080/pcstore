import { ISaveShippingMethodResource } from "./save-shipping-method-resource";

export interface ISaveShippingCompanyResource
{
    company: string;
    shippingMethods: ISaveShippingMethodResource[];
    slogan: string;
    specialNote: string;
}