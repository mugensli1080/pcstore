export interface ISaveProductResource
{
    name: string;
    description: string;
    price: number;
    stock: number;
    videoLink: string;
    availabilityId: number;
    subCategoryId: number;
}