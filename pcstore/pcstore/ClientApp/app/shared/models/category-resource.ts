import { ISubCategoryResource } from "./sub-categetory-resource";

export interface ICategoryResource
{
    id?: number;
    name: string;
    subCategories: ISubCategoryResource[];
}