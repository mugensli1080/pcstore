export interface IProductCard
{
    id: number;
    imageUrl: string;
    title: string;
    price: number;
    descripton: string;
    link: string;
    inStock: boolean;
    availability: string;
}