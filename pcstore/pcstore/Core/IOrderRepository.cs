using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IOrderRepository
    {
        Task<IEnumerable<Order>> GetOrdersByAuth0User(string auth0Id);
        Task<Order> GetOrder(int id, bool includeRelated = true);
        void Add(Order order);
        void Remove(Order order);
    }
}