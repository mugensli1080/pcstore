using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IShippingCompanyRepository
    {
        Task<IEnumerable<ShippingCompany>> GetShippingCompanies();
        Task<ShippingCompany> GetShippingCompany(int id, bool includeRelated = true);
        void Add(ShippingCompany shippingCompany);
        void Remove(ShippingCompany shippingCompany);
    }
}