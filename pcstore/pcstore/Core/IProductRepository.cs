using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(int id, bool includeRelated = true);

        Task<IEnumerable<Product>> GetNewProducts();
        void Add(Product product);
        void Remove(Product product);
    }
}