using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IAddressRepository
    {
        Task<IEnumerable<Address>> GetAddresses();
        Task<Address> GetAddress(int id);
        void Add(Address address);
        void Remove(Address address);
    }
}