using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IPaymentMethodRepository
    {
        Task<IEnumerable<PaymentMethod>> GetPaymentMethods();
        Task<PaymentMethod> GetPaymentMethod(int id);
        void Add(PaymentMethod paymentMethod);
        void Remove(PaymentMethod paymentMethod);
    }
}