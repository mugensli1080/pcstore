using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetCategories();
        Task<Category> GetCategory(int id, bool includeRelated = true);
        void Add(Category category);
        void Remove(Category category);
    }
}