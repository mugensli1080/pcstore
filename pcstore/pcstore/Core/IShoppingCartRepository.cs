using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IShoppingCartRepository
    {
        Task<ICollection<ShoppingCart>> GetUserShoppingCarts(string auth0Id);
        Task<ShoppingCart> GetCartByReference(string reference);
        Task<ShoppingCart> GetShoppingCart(int id, bool includeRelated = true);
        void Add(ShoppingCart shoppingCart);
        void Remove(ShoppingCart shoppingCart);
    }
}