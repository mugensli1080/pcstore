using System.Threading.Tasks;

namespace pcstore.Core
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}