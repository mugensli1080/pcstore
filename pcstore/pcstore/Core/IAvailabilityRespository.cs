using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IAvailabilityRespository
    {
        Task<IEnumerable<Availability>> GetAvailabilities();
        Task<Availability> GetAvailability(int id);
        void AddAvailability(Availability availability);
        void Remove(Availability availability);
    }
}