using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class ShippingMethod
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string DeliveryMethod { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        [StringLength(255)]
        public string EstimateTime { get; set; }

        [Required]
        public int ShippingCompanyId { get; set; }
        public ShippingCompany ShippingCompany { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }
    }
}