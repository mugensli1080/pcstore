using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class ShippingCompany
    {
        public int Id { get; set; }

        public int? ProductPhotoId { get; set; }
        public ProductPhoto ProductPhoto { get; set; }

        [Required]
        [StringLength(255)]
        public string Company { get; set; }

        [Required]
        public ICollection<ShippingMethod> ShippingMethods { get; set; }

        public string Slogan { get; set; }
        public string SpecialNote { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        public ShippingCompany()
        {
            ShippingMethods = new Collection<ShippingMethod>();
        }
    }
}