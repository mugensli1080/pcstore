using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class PaymentMethod
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Surcharge { get; set; }

        public string Important { get; set; }

        public string Note { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }
    }
}