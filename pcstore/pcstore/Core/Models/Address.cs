using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class Address
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        public string Company { get; set; }

        [Required]
        [StringLength(255)]
        public string Street { get; set; }

        [StringLength(255)]
        public string StreetCont { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }

    }
}