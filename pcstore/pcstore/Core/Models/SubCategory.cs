using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class SubCategory
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

    }
}