using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class ShoppingCart
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Reference { get; set; }

        [Required]
        [StringLength(255)]
        public string Auth0Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }
        public ICollection<CartItem> CartItems { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public ShoppingCart()
        {
            CartItems = new Collection<CartItem>();
        }
    }
}