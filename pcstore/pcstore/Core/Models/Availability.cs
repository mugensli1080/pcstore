using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class Availability
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public ICollection<Product> Product { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}