using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public ICollection<SubCategory> SubCategories { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public Category()
        {
            SubCategories = new Collection<SubCategory>();
        }
    }
}