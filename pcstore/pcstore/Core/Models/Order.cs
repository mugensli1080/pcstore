using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Core.Models
{
    public class Order
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Reference { get; set; }

        [Required]
        [StringLength(255)]
        public string Auth0Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        public decimal Total { get; set; }

        [Required]
        public ICollection<OrderItem> OrderItems { get; set; }

        [Required]
        public ICollection<Address> Addresses { get; set; }

        [Required]
        public int ShippingMethodId { get; set; }
        public ShippingMethod ShippingMethod { get; set; }


        [Required]
        public int PaymentMethodId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        public Order()
        {
            Addresses = new Collection<Address>();
            OrderItems = new Collection<OrderItem>();
        }

    }
}