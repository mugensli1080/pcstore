using System.Collections.Generic;
using System.Threading.Tasks;
using pcstore.Core.Models;

namespace pcstore.Core
{
    public interface IShippingMethodRepository
    {
        Task<IEnumerable<ShippingMethod>> GetShippingMethods();
        Task<ShippingMethod> GetShippingMethod(int id);
        void Add(ShippingMethod shippingMethod);
        void Remove(ShippingMethod shippingMethod);
    }
}