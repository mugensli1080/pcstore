using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class ProductRespository : IProductRepository
    {
        private readonly PcStoreDbContext _context;

        public ProductRespository(PcStoreDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            var products = await _context.Products
                                                .Include(p => p.SubCategory)
                                                    .ThenInclude(sc => sc.Category)
                                                .Include(p => p.ProductPhotos)
                                                .Include(p => p.SpecificationPhotos)
                                                .Include(p => p.Availability)
                                                .ToListAsync();
            return products;
        }

        public async Task<IEnumerable<Product>> GetNewProducts()
        {
            var products = await _context.Products
                                                .Where(p => p.Created > DateTime.Now.AddDays(-7))
                                                .Take(5)
                                                .Include(p => p.SubCategory)
                                                    .ThenInclude(sc => sc.Category)
                                                .Include(p => p.ProductPhotos)
                                                .Include(p => p.SpecificationPhotos)
                                                .Include(p => p.Availability)
                                                .ToListAsync();
            return products;
        }

        public async Task<Product> GetProduct(int id, bool includeRelated = true)
        {
            if (!includeRelated) return await _context.Products.FindAsync(id);
            var product = await _context.Products
                                                .Include(p => p.SubCategory)
                                                    .ThenInclude(sc => sc.Category)
                                                .Include(p => p.ProductPhotos)
                                                .Include(p => p.SpecificationPhotos)
                                                .Include(p => p.Availability)
                                                .SingleOrDefaultAsync(p => p.Id == id);
            return product;
        }

        public void Add(Product product)
        {
            _context.Products.Add(product);
        }

        public void Remove(Product product)
        {
            _context.Products.Remove(product);
        }
    }
}