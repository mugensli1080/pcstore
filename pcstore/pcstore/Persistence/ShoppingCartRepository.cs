using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly PcStoreDbContext _context;
        public ShoppingCartRepository(PcStoreDbContext context)
        {
            this._context = context;

        }

        public async Task<ICollection<ShoppingCart>> GetUserShoppingCarts(string auth0Id)
        {
            return await _context.ShoppingCarts
                                                .Include(x => x.CartItems).ThenInclude(x => x.Product)
                                                .Include(x => x.CartItems).ThenInclude(x => x.Product.Availability)
                                                .Include(x => x.CartItems).ThenInclude(x => x.Product.ProductPhotos)
                                                .Where(c => c.Auth0Id == auth0Id).ToListAsync();
        }

        public async Task<ShoppingCart> GetCartByReference(string reference)
        {
            return await _context.ShoppingCarts.SingleOrDefaultAsync(c => c.Reference == reference);
        }

        public async Task<ShoppingCart> GetShoppingCart(int id, bool includeRelated = true)
        {
            if (!includeRelated) return await _context.ShoppingCarts.FindAsync(id);

            var cart = await _context.ShoppingCarts
                                                    .Include(c => c.CartItems).ThenInclude(p => p.Product)
                                                    .Include(c => c.CartItems).ThenInclude(p => p.Product.Availability)
                                                    .Include(c => c.CartItems).ThenInclude(p => p.Product.ProductPhotos)
                                                    .SingleOrDefaultAsync(c => c.Id == id);
            return cart;
        }

        public void Add(ShoppingCart shoppingCart)
        {
            _context.ShoppingCarts.Add(shoppingCart);
        }

        public void Remove(ShoppingCart shoppingCart)
        {
            _context.ShoppingCarts.Remove(shoppingCart);
        }

    }
}