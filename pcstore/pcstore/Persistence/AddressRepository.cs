using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class AddressRepository : IAddressRepository
    {
        private readonly PcStoreDbContext _context;
        public AddressRepository(PcStoreDbContext context)
        {
            this._context = context;

        }

        public async Task<IEnumerable<Address>> GetAddresses()
        {
            return await _context.Addresses.ToListAsync();
        }

        public async Task<Address> GetAddress(int id)
        {
            return await _context.Addresses.FindAsync(id);
        }


        public void Add(Address address)
        {
            _context.Addresses.Add(address);
        }

        public void Remove(Address address)
        {
            _context.Addresses.Remove(address);
        }
    }
}