using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;
using pcstore.Persistence;

namespace pcstore.Persistence
{
    public class ShippingMethodRepository : IShippingMethodRepository
    {
        private readonly PcStoreDbContext _context;
        public ShippingMethodRepository(PcStoreDbContext context)
        {
            this._context = context;

        }

        public async Task<IEnumerable<ShippingMethod>> GetShippingMethods()
        {
            return await _context.ShippingMethods.ToListAsync();
        }

        public async Task<ShippingMethod> GetShippingMethod(int id)
        {
            return await _context.ShippingMethods.FindAsync(id);
        }

        public void Add(ShippingMethod shippingMethod)
        {
            _context.ShippingMethods.Add(shippingMethod);
        }

        public void Remove(ShippingMethod shippingMethod)
        {
            _context.ShippingMethods.Remove(shippingMethod);
        }
    }
}