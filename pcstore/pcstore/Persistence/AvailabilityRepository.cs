using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class AvailabilityRepository : IAvailabilityRespository
    {
        private readonly PcStoreDbContext _context;
        public AvailabilityRepository(PcStoreDbContext context)
        {
            this._context = context;

        }

        public async Task<IEnumerable<Availability>> GetAvailabilities()
        {
            return await _context.Availabilities.ToListAsync();
        }

        public async Task<Availability> GetAvailability(int id)
        {
            return await _context.Availabilities.FindAsync(id);
        }

        public void AddAvailability(Availability availability)
        {
            _context.Availabilities.Add(availability);
        }

        public void Remove(Availability availability)
        {
            _context.Remove(availability);
        }
    }
}