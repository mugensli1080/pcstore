using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class ShippingCompanyRepository : IShippingCompanyRepository
    {
        private readonly PcStoreDbContext _context;
        public ShippingCompanyRepository(PcStoreDbContext context)
        {
            this._context = context;

        }

        public async Task<IEnumerable<ShippingCompany>> GetShippingCompanies()
        {
            return await _context.ShippingCompanies
                                                    .Include(cm => cm.ShippingMethods)
                                                    .ToListAsync();

        }

        public async Task<ShippingCompany> GetShippingCompany(int id, bool includeRelated = true)
        {
            if (includeRelated == false) return await _context.ShippingCompanies.FindAsync(id);
            return await _context.ShippingCompanies
                                                    .Include(sc => sc.ShippingMethods)
                                                    .SingleOrDefaultAsync(x => x.Id == id);
        }
        public void Add(ShippingCompany shippingCompany)
        {
            _context.ShippingCompanies.Add(shippingCompany);
        }

        public void Remove(ShippingCompany shippingCompany)
        {
            _context.ShippingCompanies.Remove(shippingCompany);
        }
    }
}