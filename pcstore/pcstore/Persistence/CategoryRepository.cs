using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly PcStoreDbContext _context;
        public CategoryRepository(PcStoreDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _context.Categories
                                            .Include(c => c.SubCategories)
                                            .ToListAsync();
        }

        public async Task<Category> GetCategory(int id, bool includeRelated = true)
        {
            if (includeRelated == false) return await _context.Categories.FindAsync(id);
            return await _context.Categories
                                            .Include(c => c.SubCategories)
                                            .SingleOrDefaultAsync(c => c.Id == id);
        }

        public void Add(Category category)
        {
            _context.Categories.Add(category);
        }

        public void Remove(Category category)
        {
            _context.Categories.Remove(category);
        }
    }
}