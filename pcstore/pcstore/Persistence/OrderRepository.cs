using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class OrderRepository : IOrderRepository
    {
        private readonly PcStoreDbContext _context;
        public OrderRepository(PcStoreDbContext context)
        {
            this._context = context;

        }



        public async Task<IEnumerable<Order>> GetOrdersByAuth0User(string auth0Id)
        {
            return await _context.Orders
                                        .Include(o => o.OrderItems)
                                        .Include(o => o.Addresses)
                                        .Include(o => o.ShippingMethod)
                                        .Include(o => o.PaymentMethod)
                                        .Where(o => o.Auth0Id == auth0Id).ToListAsync();
        }

        public async Task<Order> GetOrder(int id, bool includeRelated = true)
        {
            if (includeRelated == false) return await _context.Orders.FindAsync(id);
            return await _context.Orders
                                        .Include(o => o.OrderItems)
                                        .Include(o => o.Addresses)
                                        .Include(o => o.ShippingMethod)
                                        .Include(o => o.PaymentMethod)
                                        .SingleOrDefaultAsync(o => o.Id == id);
        }

        public void Add(Order order)
        {
            _context.Orders.Add(order);
        }

        public void Remove(Order order)
        {
            _context.Remove(order);
        }
    }
}