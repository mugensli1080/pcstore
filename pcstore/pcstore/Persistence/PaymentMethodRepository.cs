using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Persistence
{
    public class PaymentMethodRepository : IPaymentMethodRepository
    {
        private readonly PcStoreDbContext _context;
        public PaymentMethodRepository(PcStoreDbContext context)
        {
            this._context = context;

        }
        public async Task<IEnumerable<PaymentMethod>> GetPaymentMethods()
        {
            return await _context.PaymentMethods.ToListAsync();
        }

        public async Task<PaymentMethod> GetPaymentMethod(int id)
        {
            return await _context.PaymentMethods.FindAsync(id);
        }

        public void Add(PaymentMethod paymentMethod)
        {
            _context.PaymentMethods.Add(paymentMethod);
        }

        public void Remove(PaymentMethod paymentMethod)
        {
            _context.PaymentMethods.Remove(paymentMethod);
        }
    }
}