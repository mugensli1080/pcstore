﻿using AutoMapper;
using pcstore.Controllers.Resources;
using pcstore.Controllers.Resources.Save;
using pcstore.Controllers.Resources.View;
using pcstore.Core.Models;
using System.Linq;

namespace pcstore.Mapping
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            MapAddresses();
            MapAvailabilities();
            MapPhotos();
            MapPaymentMethods();
            MapProducts();

            MapShoppingCart();
            MapCartItem();

            MapOrders();
            MapOrderItems();

            MapCategories();
            MapSubCategories();

            MapShippingCompanies();
            MapShippingMethods();
        }

        private void MapAvailabilities()
        {
            //? Domain to API

            CreateMap<Availability, ViewAvailabilityResource>();

            //! API to Domain

            CreateMap<SaveAvailabilityResource, Availability>()
            .ForMember( ar => ar.Id, opt => opt.Ignore() );
        }

        private void MapPaymentMethods()
        {
            //? Domain to API

            CreateMap<PaymentMethod, ViewPaymentMethodResource>();

            //! API to Domain

            CreateMap<SavePaymentMethodResource, PaymentMethod>()
            .ForMember( pr => pr.Id, opt => opt.Ignore() );

        }

        private void MapShippingMethods()
        {
            //? Domain to API

            CreateMap<ShippingMethod, ViewShippingMethodResource>();

            //! API to Domain

            CreateMap<SaveShippingMethodResource, ShippingMethod>()
            .ForMember( sm => sm.Id, opt => opt.Ignore() );
        }

        private void MapSubCategories()
        {
            //? Domain to API

            CreateMap<SubCategory, SubCategoryResource>();

            //! API to Domain

            CreateMap<SubCategoryResource, SubCategory>()
            .ForMember( scr => scr.Id, opt => opt.Ignore() )
            .ForMember( scr => scr.Category, opt => opt.Ignore() );
        }

        private void MapCategories()
        {
            //? Domain to API

            CreateMap<Category, CategoryResource>();

            //! API to Domain

            CreateMap<CategoryResource, Category>()
            .ForMember( c => c.Id, opt => opt.Ignore() );
        }

        private void MapShippingCompanies()
        {
            //? Domain to API

            CreateMap<ShippingCompany, ViewShippingCompanyResource>();

            //! API to Domain

            CreateMap<SaveShippingCompanyResource, ShippingCompany>()
            .ForMember( sc => sc.Id, opt => opt.Ignore() );

        }
        private void MapOrders()
        {
            //? Domain to API

            CreateMap<Order, ViewOrderResource>()
            .ForMember( v => v.TotalPrice, opt => opt.MapFrom( o => o.OrderItems.Sum( i => i.SubTotal ) ) );

            //! API to Domain

            CreateMap<SaveOrderResource, Order>()
            .ForMember( so => so.Id, opt => opt.Ignore() );
        }

        private void MapOrderItems()
        {
            //? Domain to API

            CreateMap<OrderItem, ViewOrderItemResource>()
            .ForMember( v => v.Thumbnail, opt => opt.MapFrom( o => o.Product.ProductPhotos.SingleOrDefault( p => p.Activated ).Thumbnail ) );

            //! API to Domain

            CreateMap<SaveOrderItemResource, OrderItem>()
            .ForMember( oi => oi.Id, opt => opt.Ignore() );
        }

        private void MapAddresses()
        {
            //? Domain to API

            CreateMap<Address, ViewAddressResource>();

            //! API to Domain

            CreateMap<SaveAddressResource, Address>()
            .ForMember( sa => sa.Id, opt => opt.Ignore() );
        }

        private void MapPhotos()
        {
            //? Domain To API

            CreateMap<ProductPhoto, ViewProductPhotoResource>();
            CreateMap<SpecificationPhoto, ViewSpecificationPhotoResource>();

            //! API to Domain

            CreateMap<SaveProductPhotoResource, ProductPhoto>();
            CreateMap<SaveSpecificationPhotoResource, SpecificationPhoto>();
        }

        private void MapShoppingCart()
        {
            //? Domain To API

            CreateMap<ShoppingCart, ViewShoppingCartResource>()
            .ForMember( v => v.TotalPrice, opt => opt.MapFrom( c => c.CartItems.Sum( i => i.Quantity * i.Product.Price ) ) );

            //! API to Domain

            CreateMap<SaveShoppingCartResource, ShoppingCart>()
            .ForMember( c => c.Id, opt => opt.Ignore() );
        }

        private void MapCartItem()
        {
            //? Domain To API

            CreateMap<CartItem, ViewCartItemResource>()
            .ForMember( v => v.ProductName, opt => opt.MapFrom( c => c.Product.Name ) )
            .ForMember( v => v.UnitPrice, opt => opt.MapFrom( c => c.Product.Price ) )
            .ForMember( v => v.Quantity, opt => opt.MapFrom( c => c.Quantity ) )
            .ForMember( v => v.Availability, opt => opt.MapFrom( c => c.Product.Availability.Name ) )
            .ForMember( v => v.Thumbnail, opt => opt.MapFrom( c => c.Product.ProductPhotos.SingleOrDefault( p => p.Activated ).Thumbnail ) )
            .ForMember( v => v.SubTotal, opt => opt.MapFrom( c => c.Quantity * c.Product.Price ) );

            //! API to Domain

            CreateMap<SaveCartItemResource, CartItem>()
            .ForMember( ir => ir.Id, opt => opt.Ignore() );
        }

        private void MapProducts()
        {
            //? Domain To API

            CreateMap<Product, ViewProductResource>()
            .ForMember( p => p.CategoryId, opt => opt.MapFrom( x => x.SubCategory.Category.Id ) )
            .ForMember( p => p.CategoryName, opt => opt.MapFrom( x => x.SubCategory.Category.Name ) )
            .ForMember( p => p.SubCategoryId, opt => opt.MapFrom( x => x.SubCategoryId ) )
            .ForMember( p => p.SubCategoryName, opt => opt.MapFrom( x => x.SubCategory.Name ) );

            //! API to Domain

            CreateMap<SaveProductResource, Product>()
            .ForMember( spr => spr.Id, opt => opt.Ignore() );
        }
    }
}
