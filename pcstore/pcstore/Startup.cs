using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using pcstore.Core;
using pcstore.Persistence;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using pcstore.Controllers;

namespace pcstore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services
                    .AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(options =>
                    {
                        options.Authority = domain;
                        options.Audience = Configuration["Auth0:ApiIdentifier"];
                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AppPolicies.RequiredAdminRole, policy => policy.RequireClaim("https://pc_store.com/roles", "Admin"));
                options.AddPolicy(AppPolicies.RequireModeratorRole, policy => policy.RequireClaim("https://pc_store.com/roles", "Moderator"));
            });

            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IShippingCompanyRepository, ShippingCompanyRepository>();
            services.AddScoped<IShippingMethodRepository, ShippingMethodRepository>();
            services.AddScoped<IPaymentMethodRepository, PaymentMethodRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IAvailabilityRespository, AvailabilityRepository>();
            services.AddScoped<IShoppingCartRepository, ShoppingCartRepository>();

            services.AddTransient<IPhotoStorage, FileSystemPhotoStorage>();
            services.AddTransient<IPhotoService, PhotoService>();

            services.Configure<PhotoSettings>(Configuration.GetSection("PhotoSettings"));
            services.AddScoped<ISubCategoryRepository, SubCategoryRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRespository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<PcStoreDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")));

            services.AddAutoMapper();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
