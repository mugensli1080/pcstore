﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace pcstore.Migrations
{
    public partial class SeedSpecPhotos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT[dbo].[SpecificationPhotos] ([Created], [Index], [Modified], [Name], [ProductId], [Thumbnail]) VALUES(CAST(N'2017-12-31T16:37:48.5455814' AS DateTime2), 0, CAST(N'2017-12-31T16:37:48.5456487' AS DateTime2), N'baaf5fcd-68d6-415b-8f65-bb1b04126396.jpg', 16, N'a9df26b5-aed6-40e1-8f7b-fa3bda79334e.jpg')");
            migrationBuilder.Sql("INSERT[dbo].[SpecificationPhotos] ([Created], [Index], [Modified], [Name], [ProductId], [Thumbnail]) VALUES(CAST(N'2017-12-31T18:33:34.5790174' AS DateTime2), 0, CAST(N'2017-12-31T18:33:34.5790747' AS DateTime2), N'd1319312-e482-425a-9719-297323e1efca.jpg', 19, N'da17f5e6-d207-4308-9284-3926c0aef008.jpg')");
            migrationBuilder.Sql("INSERT[dbo].[SpecificationPhotos] ([Created], [Index], [Modified], [Name], [ProductId], [Thumbnail]) VALUES(CAST(N'2017-12-31T18:34:41.8747763' AS DateTime2), 0, CAST(N'2017-12-31T18:34:41.8747822' AS DateTime2), N'fa3066c6-d01c-4cc7-bb5c-61cb86e57b75.jpg', 18, N'9d5477ca-0f19-491d-9114-224475fbfeba.jpg')");
            migrationBuilder.Sql("INSERT[dbo].[SpecificationPhotos] ([Created], [Index], [Modified], [Name], [ProductId], [Thumbnail]) VALUES(CAST(N'2017-12-31T18:36:52.3177275' AS DateTime2), 0, CAST(N'2017-12-31T18:36:52.3177342' AS DateTime2), N'74ec7aa0-cf4b-42d2-a34f-f0a99bf382a4.jpg', 17, N'2c1b3f25-b23f-4506-8272-7ecd9ce7a3d7.jpg')");
            migrationBuilder.Sql("INSERT[dbo].[SpecificationPhotos] ([Created], [Index], [Modified], [Name], [ProductId], [Thumbnail]) VALUES(CAST(N'2017-12-31T18:40:17.0405859' AS DateTime2), 0, CAST(N'2017-12-31T18:40:17.0405924' AS DateTime2), N'724de63d-aa94-430e-8dc5-b35bd0d36224.png', 4, N'86446942-3675-4525-8a4d-ff672f33a418.png')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM ProductPhotos WHERE Thumbnail IN ('a9df26b5-aed6-40e1-8f7b-fa3bda79334e.jpg','da17f5e6-d207-4308-9284-3926c0aef008.jpg','9d5477ca-0f19-491d-9114-224475fbfeba.jpg','2c1b3f25-b23f-4506-8272-7ecd9ce7a3d7.jpg','86446942-3675-4525-8a4d-ff672f33a418.png')");
        }
    }
}
