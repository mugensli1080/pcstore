using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.View;
using pcstore.Controllers.Resources.Save;
using pcstore.Core;
using pcstore.Core.Models;
using System;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/shippingcompanies")]
    public class ShippingCompaniesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IShippingCompanyRepository _repository;
        public ShippingCompaniesController(IShippingCompanyRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._repository = repository;
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;

        }

        [HttpGet]
        public async Task<IActionResult> GetShippingCompanies()
        {
            var shippingcompanies = await _repository.GetShippingCompanies();
            return Ok(_mapper.Map<IEnumerable<ShippingCompany>, IEnumerable<ViewShippingCompanyResource>>(shippingcompanies));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetShippingCompany(int id)
        {
            var shippingcompany = await _repository.GetShippingCompany(id);
            if (shippingcompany == null) return NotFound();
            return Ok(_mapper.Map<ShippingCompany, ViewShippingCompanyResource>(shippingcompany));
        }

        [HttpPost]
        public async Task<IActionResult> CreateShippingCompany([FromBody] SaveShippingCompanyResource saveShippingCompanyResource)
        {
            if (!ModelState.IsValid) return BadRequest();
            var shippingCompany = _mapper.Map<SaveShippingCompanyResource, ShippingCompany>(saveShippingCompanyResource);
            shippingCompany.Created = DateTime.Now;
            shippingCompany.Modified = DateTime.Now;

            _repository.Add(shippingCompany);
            await _unitOfWork.CompleteAsync();

            shippingCompany = await _repository.GetShippingCompany(shippingCompany.Id);
            return Ok(_mapper.Map<ShippingCompany, ViewShippingCompanyResource>(shippingCompany));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateShippingCompany(int id, [FromBody] SaveShippingCompanyResource saveShippingCompanyResource)
        {
            if (!ModelState.IsValid) return BadRequest();
            var shippingCompany = await _repository.GetShippingCompany(id);
            if (shippingCompany == null) return NotFound();

            _mapper.Map<SaveShippingCompanyResource, ShippingCompany>(saveShippingCompanyResource, shippingCompany);
            shippingCompany.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            shippingCompany = await _repository.GetShippingCompany(id);
            return Ok(_mapper.Map<ShippingCompany, ViewShippingCompanyResource>(shippingCompany));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShippingCompany(int id)
        {
            var shippingCompany = await _repository.GetShippingCompany(id, includeRelated: false);
            if (shippingCompany == null) return NotFound();
            _repository.Remove(shippingCompany);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}