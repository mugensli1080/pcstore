using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.Save;
using pcstore.Controllers.Resources.View;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/availabilities")]
    public class AvailabilitiesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAvailabilityRespository _repository;
        private readonly IUnitOfWork _unitOfWork;
        public AvailabilitiesController(IUnitOfWork unitOfWork, IMapper mapper, IAvailabilityRespository _repository)
        {
            this._unitOfWork = unitOfWork;
            this._repository = _repository;
            this._mapper = mapper;

        }

        [HttpGet]
        public async Task<IActionResult> GetAvailabilities()
        {
            var availabilities = await _repository.GetAvailabilities();
            return Ok(_mapper.Map<IEnumerable<Availability>, IEnumerable<ViewAvailabilityResource>>(availabilities));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAvailability(int id)
        {
            var availability = await _repository.GetAvailability(id);
            if (availability == null) return NotFound();

            return Ok(_mapper.Map<Availability, ViewAvailabilityResource>(availability));
        }

        [HttpPost]
        public async Task<IActionResult> CreateAvailability([FromBody] SaveAvailabilityResource availabilityResource)
        {
            if (!ModelState.IsValid) return BadRequest();

            var availability = _mapper.Map<SaveAvailabilityResource, Availability>(availabilityResource);
            availability.Created = DateTime.Now;
            availability.Modified = DateTime.Now;

            _repository.AddAvailability(availability);
            await _unitOfWork.CompleteAsync();

            availability = await _repository.GetAvailability(availability.Id);
            return Ok(_mapper.Map<Availability, ViewAvailabilityResource>(availability));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAvailability(int id, [FromBody] SaveAvailabilityResource availabilityResource)
        {
            if (!ModelState.IsValid) return BadRequest();

            var availability = await _repository.GetAvailability(id);
            if (availability == null) return NotFound();

            _mapper.Map<SaveAvailabilityResource, Availability>(availabilityResource, availability);
            availability.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            availability = await _repository.GetAvailability(id);
            return Ok(_mapper.Map<Availability, ViewAvailabilityResource>(availability));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAvailability(int id)
        {
            var availability = await _repository.GetAvailability(id);
            if (availability == null) return NotFound();
            _repository.Remove(availability);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}