using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources
{
    public class CategoryResource
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<SubCategoryResource> SubCategories { get; set; }

        public CategoryResource()
        {
            SubCategories = new Collection<SubCategoryResource>();
        }
    }
}