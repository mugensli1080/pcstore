using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources
{
    public class SubCategoryResource
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public int CategoryId { get; set; }
    }
}