using System;

namespace pcstore.Controllers.Resources.View
{
    public class ViewAddressResource
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Company { get; set; }
        public string Street { get; set; }
        public string StreetCont { get; set; }
        public string PostCode { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Type { get; set; }
    }
}