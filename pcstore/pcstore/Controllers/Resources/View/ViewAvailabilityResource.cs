using System;

namespace pcstore.Controllers.Resources.View
{
    public class ViewAvailabilityResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}