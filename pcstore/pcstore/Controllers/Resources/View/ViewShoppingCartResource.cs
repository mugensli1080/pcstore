using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace pcstore.Controllers.Resources.View
{
    public class ViewShoppingCartResource
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string Auth0Id { get; set; }
        public string Email { get; set; }
        public ICollection<ViewCartItemResource> CartItems { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public ViewShoppingCartResource()
        {
            CartItems = new Collection<ViewCartItemResource>();
        }
    }
}