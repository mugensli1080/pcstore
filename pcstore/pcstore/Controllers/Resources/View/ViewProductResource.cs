using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace pcstore.Controllers.Resources.View
{
    public class ViewProductResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public int AvailabilityId { get; set; }
        public string AvailabilityName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public ICollection<ViewProductPhotoResource> ProductPhotos { get; set; }
        public ICollection<ViewSpecificationPhotoResource> SpecificationPhotos { get; set; }
        public string VideoLink { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public ViewProductResource()
        {
            ProductPhotos = new Collection<ViewProductPhotoResource>();
            SpecificationPhotos = new Collection<ViewSpecificationPhotoResource>();
        }
    }
}