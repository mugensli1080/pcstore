using System;

namespace pcstore.Controllers.Resources.View
{
    public class ViewProductPhotoResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public int ProductId { get; set; }
        public bool Activated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}