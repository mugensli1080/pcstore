using System;

namespace pcstore.Controllers.Resources.View
{
    public class ViewPaymentMethodResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Surcharge { get; set; }
        public string Important { get; set; }
        public string Note { get; set; }
    }
}