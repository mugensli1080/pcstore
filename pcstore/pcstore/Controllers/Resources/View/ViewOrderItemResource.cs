namespace pcstore.Controllers.Resources.View
{
    public class ViewOrderItemResource
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal SubTotal { get; set; }
        public string Thumbnail { get; set; }
    }
}