using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace pcstore.Controllers.Resources.View
{
    public class ViewShippingCompanyResource
    {
        public int Id { get; set; }
        public ViewProductPhotoResource ProductPhoto { get; set; }
        public string Company { get; set; }
        public ICollection<ViewShippingMethodResource> ShippingMethods { get; set; }
        public string Slogan { get; set; }
        public string SpecialNote { get; set; }

        public ViewShippingCompanyResource()
        {
            ShippingMethods = new Collection<ViewShippingMethodResource>();
        }
    }
}