using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace pcstore.Controllers.Resources.View
{
    public class ViewOrderResource
    {

        public int Id { get; set; }
        public string Reference { get; set; }
        public string Auth0Id { get; set; }
        public string Email { get; set; }
        public ICollection<ViewOrderItemResource> OrderItems { get; set; }
        public decimal TotalPrice { get; set; }
        public ICollection<ViewAddressResource> Addresses { get; set; }
        public ViewShippingMethodResource ShippingMethod { get; set; }
        public ViewPaymentMethodResource PaymentMethod { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public ViewOrderResource()
        {
            Addresses = new Collection<ViewAddressResource>();
            OrderItems = new Collection<ViewOrderItemResource>();
        }
    }
}