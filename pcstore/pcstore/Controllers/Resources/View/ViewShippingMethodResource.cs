using System;

namespace pcstore.Controllers.Resources.View
{
    public class ViewShippingMethodResource
    {
        public int Id { get; set; }
        public string DeliveryMethod { get; set; }
        public decimal Price { get; set; }
        public string EstimateTime { get; set; }
        public int ShippingCompanyId { get; set; }
    }
}