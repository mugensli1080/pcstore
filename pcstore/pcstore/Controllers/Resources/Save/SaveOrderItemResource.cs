using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveOrderItemResource
    {

        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}