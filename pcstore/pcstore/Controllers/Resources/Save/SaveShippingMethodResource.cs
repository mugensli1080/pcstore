using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveShippingMethodResource
    {
        [Required]
        [StringLength(255)]
        public string DeliveryMethod { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        [StringLength(255)]
        public string EstimateTime { get; set; }
    }
}