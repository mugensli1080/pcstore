using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveOrderResource
    {
        [Required]
        [StringLength(255)]
        public string Auth0Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Reference { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        public ICollection<SaveOrderItemResource> OrderItems { get; set; }

        [Required]
        public ICollection<SaveAddressResource> Addresses { get; set; }

        [Required]
        public int ShippingMethodId { get; set; }

        [Required]
        public int PaymentMethodId { get; set; }
    }
}