using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveAvailabilityResource
    {
        [Required]
        public string Name { get; set; }
    }
}