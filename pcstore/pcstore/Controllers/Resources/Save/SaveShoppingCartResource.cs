using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using pcstore.Core.Models;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveShoppingCartResource
    {
        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(255)]
        public string Auth0Id { get; set; }

        [Required]
        public ICollection<SaveCartItemResource> CartItems { get; set; }
    }
}