using System;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SavePaymentMethodResource
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Surcharge { get; set; }

        public string Important { get; set; }

        public string Note { get; set; }
    }
}