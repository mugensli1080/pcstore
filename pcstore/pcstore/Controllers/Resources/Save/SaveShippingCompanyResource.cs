using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveShippingCompanyResource
    {
        [Required]
        [StringLength(255)]
        public string Company { get; set; }

        [Required]
        public ICollection<SaveShippingMethodResource> ShippingMethods { get; set; }

        public string Slogan { get; set; }
        public string SpecialNote { get; set; }

        public SaveShippingCompanyResource()
        {
            ShippingMethods = new Collection<SaveShippingMethodResource>();
        }
    }
}