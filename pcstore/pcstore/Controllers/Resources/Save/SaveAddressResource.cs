using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveAddressResource
    {
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        public string Company { get; set; }

        [Required]
        [StringLength(255)]
        public string Street { get; set; }

        [StringLength(255)]
        public string StreetCont { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public int OrderId { get; set; }
        public SaveOrderItemResource Order { get; set; }
    }
}