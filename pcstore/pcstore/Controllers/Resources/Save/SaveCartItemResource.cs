using System.ComponentModel.DataAnnotations;
using pcstore.Controllers.Resources.Save;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveCartItemResource
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}