using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveProductPhotoResource
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public int ProductId { get; set; }

        public bool Activated { get; set; }
    }
}