using System.ComponentModel.DataAnnotations;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveSpecificationPhotoResource
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public int ProductId { get; set; }

        public int Index { get; set; }
    }
}