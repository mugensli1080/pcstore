using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using pcstore.Core.Models;

namespace pcstore.Controllers.Resources.Save
{
    public class SaveProductResource
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Stock { get; set; }

        public string VideoLink { get; set; }

        [Required]
        public int AvailabilityId { get; set; }

        [Required]
        public int SubCategoryId { get; set; }
    }
}