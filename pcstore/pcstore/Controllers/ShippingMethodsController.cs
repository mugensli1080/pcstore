using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources;
using pcstore.Controllers.Resources.View;
using pcstore.Controllers.Resources.Save;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/shippingmethods")]
    public class ShippingMethodsController : Controller
    {
        private readonly IShippingMethodRepository _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public ShippingMethodsController(IShippingMethodRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._repository = repository;

        }

        [HttpGet]
        public async Task<IActionResult> GetShippingMethods()
        {
            var shippingMethods = await _repository.GetShippingMethods();
            return Ok(_mapper.Map<IEnumerable<ShippingMethod>, IEnumerable<ViewShippingMethodResource>>(shippingMethods));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetShippingMethod(int id)
        {
            var shippingMethod = await _repository.GetShippingMethod(id);
            if (shippingMethod == null) return NotFound();
            return Ok(_mapper.Map<ShippingMethod, ViewShippingMethodResource>(shippingMethod));
        }

        [HttpPost]
        public async Task<IActionResult> CreateShippingMethod([FromBody] SaveShippingMethodResource shippingMethodResource)
        {
            if (!ModelState.IsValid) return BadRequest();
            var shippingMethod = _mapper.Map<SaveShippingMethodResource, ShippingMethod>(shippingMethodResource);
            shippingMethod.Created = DateTime.Now;
            shippingMethod.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            shippingMethod = await _repository.GetShippingMethod(shippingMethod.Id);
            return Ok(_mapper.Map<ShippingMethod, ViewShippingMethodResource>(shippingMethod));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateShippingMethod(int id, [FromBody] SaveShippingMethodResource shippingMethodResource)
        {
            if (!ModelState.IsValid) return BadRequest();
            var shippingMethod = await _repository.GetShippingMethod(id);

            _mapper.Map<SaveShippingMethodResource, ShippingMethod>(shippingMethodResource, shippingMethod);
            shippingMethod.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            shippingMethod = await _repository.GetShippingMethod(id);
            return Ok(_mapper.Map<ShippingMethod, ViewShippingMethodResource>(shippingMethod));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid) return BadRequest();
            var shippingMethod = await _repository.GetShippingMethod(id);
            if (shippingMethod == null) return NotFound();
            _repository.Remove(shippingMethod);

            return Ok(id);
        }
    }
}