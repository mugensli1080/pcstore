using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.Save;
using pcstore.Controllers.Resources.View;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/shoppingcarts")]
    public class ShoppingCartsController : Controller
    {
        private readonly int _MIN = 1000000;
        private readonly int _MAX = 9999999;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductRepository _productRepository;
        private readonly IShoppingCartRepository _shoppingCartRepository;

        public ShoppingCartsController(IMapper mapper,
        IUnitOfWork unitOfWork,
        IProductRepository productRepository,
        IShoppingCartRepository shoppingCartRepository)
        {
            this._shoppingCartRepository = shoppingCartRepository;
            this._productRepository = productRepository;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;

        }

        [HttpGet("user/{auth0Id}")]
        public async Task<IActionResult> GetUserShoppingCarts(string auth0Id)
        {
            var shoppingCarts = await _shoppingCartRepository.GetUserShoppingCarts(auth0Id);
            return Ok(_mapper.Map<IEnumerable<ShoppingCart>, IEnumerable<ViewShoppingCartResource>>(shoppingCarts));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetShoppingCart(int id)
        {
            var shoppingCart = await _shoppingCartRepository.GetShoppingCart(id);
            if (shoppingCart == null) return NotFound();
            return Ok(_mapper.Map<ShoppingCart, ViewShoppingCartResource>(shoppingCart));
        }

        [HttpPost]
        public async Task<IActionResult> CreateShoppingCart([FromBody] SaveShoppingCartResource saveShoppingCart)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var cart = _mapper.Map<SaveShoppingCartResource, ShoppingCart>(saveShoppingCart);
            var random = new Random();
            cart.Reference = random.Next(_MIN, _MAX).ToString();

            cart.Created = DateTime.Now;
            cart.Modified = DateTime.Now;

            _shoppingCartRepository.Add(cart);
            await _unitOfWork.CompleteAsync();

            cart = await _shoppingCartRepository.GetShoppingCart(cart.Id);
            return Ok(_mapper.Map<ShoppingCart, ViewShoppingCartResource>(cart));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateShoppingCart(int id, [FromBody] SaveShoppingCartResource saveShoppingCart)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var cart = await _shoppingCartRepository.GetShoppingCart(id);
            if (cart == null) return NotFound();

            _mapper.Map<SaveShoppingCartResource, ShoppingCart>(saveShoppingCart, cart);
            cart.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            cart = await _shoppingCartRepository.GetShoppingCart(id);
            return Ok(_mapper.Map<ShoppingCart, ViewShoppingCartResource>(cart));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShoppingCart(int id)
        {
            var cart = await _shoppingCartRepository.GetShoppingCart(id, includeRelated: false);
            if (cart == null) return NotFound();

            _shoppingCartRepository.Remove(cart);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}