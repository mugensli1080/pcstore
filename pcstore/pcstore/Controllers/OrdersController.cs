using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.View;
using pcstore.Controllers.Resources.Save;
using pcstore.Core;
using pcstore.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/orders")]
    public class OrdersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _repository;
        private readonly IProductRepository _productRepository;
        private readonly IShoppingCartRepository _shoppingCartRepository;
        public OrdersController(IOrderRepository repository, IProductRepository productRepository, IShoppingCartRepository shoppingCartRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._shoppingCartRepository = shoppingCartRepository;
            this._productRepository = productRepository;
            this._repository = repository;
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;

        }

        [HttpGet("user/{auth0Id}")]
        public async Task<IActionResult> GetOrders(string auth0Id)
        {
            var orders = await _repository.GetOrdersByAuth0User(auth0Id);
            return Ok(_mapper.Map<IEnumerable<Order>, IEnumerable<ViewOrderResource>>(orders));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrder(int id)
        {
            var order = await _repository.GetOrder(id);
            if (order == null) return NotFound();
            return Ok(_mapper.Map<Order, ViewOrderResource>(order));
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder([FromBody] SaveOrderResource saveOrderResource)
        {
            if (!ModelState.IsValid) return BadRequest();

            var order = _mapper.Map<SaveOrderResource, Order>(saveOrderResource);

            foreach (var item in order.OrderItems)
            {
                var product = await _productRepository.GetProduct(item.ProductId);
                item.ProductName = product.Name;
                item.UnitPrice = product.Price;
                item.SubTotal = item.Quantity * product.Price;
            }
            order.Total = order.OrderItems.Sum(i => i.SubTotal);

            order.Created = DateTime.Now;
            order.Modified = DateTime.Now;

            _repository.Add(order);

            var shoppingCart = await _shoppingCartRepository.GetCartByReference(order.Reference);
            if (shoppingCart != null) _shoppingCartRepository.Remove(shoppingCart);

            await _unitOfWork.CompleteAsync();

            order = await _repository.GetOrder(order.Id);
            return Ok(_mapper.Map<Order, ViewOrderResource>(order));
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder(int id, [FromBody] SaveOrderResource saveOrderResource)
        {
            if (!ModelState.IsValid) return BadRequest();
            var order = await _repository.GetOrder(id);
            if (order == null) return NotFound();

            _mapper.Map<SaveOrderResource, Order>(saveOrderResource, order);
            order.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            order = await _repository.GetOrder(id);
            return Ok(_mapper.Map<Order, ViewOrderResource>(order));
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var order = await _repository.GetOrder(id, includeRelated: false);
            if (order == null) return NotFound();
            _repository.Remove(order);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}