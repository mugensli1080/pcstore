using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.Save;
using pcstore.Controllers.Resources.View;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/addresses")]
    public class AddressesController : Controller
    {
        private readonly IAddressRepository _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public AddressesController(IAddressRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._repository = repository;

        }

        [HttpGet]
        public async Task<IActionResult> GetAddresses()
        {
            var addresses = await _repository.GetAddresses();
            return Ok(_mapper.Map<IEnumerable<Address>, IEnumerable<ViewAddressResource>>(addresses));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAddress(int id)
        {
            var address = await _repository.GetAddress(id);
            if (address == null) return NotFound();

            return Ok(_mapper.Map<Address, ViewAddressResource>(address));
        }

        [HttpPost]
        public async Task<IActionResult> CreateAddress([FromBody] SaveAddressResource saveAddressResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var address = _mapper.Map<SaveAddressResource, Address>(saveAddressResource);
            address.Created = DateTime.Now;
            address.Modified = DateTime.Now;

            _repository.Add(address);
            await _unitOfWork.CompleteAsync();

            var result = await _repository.GetAddress(address.Id);
            return Ok(_mapper.Map<Address, ViewAddressResource>(result));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAddress(int id, [FromBody] SaveAddressResource saveAddressResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var address = await _repository.GetAddress(id);
            if (address == null) return NotFound();

            _mapper.Map<SaveAddressResource, Address>(saveAddressResource, address);
            address.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            address = await _repository.GetAddress(id);
            return Ok(_mapper.Map<Address, ViewAddressResource>(address));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAddress(int id)
        {
            var address = await _repository.GetAddress(id);
            if (address == null) return NotFound();
            _repository.Remove(address);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}