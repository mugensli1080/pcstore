using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pcstore.Controllers.Resources.View;
using pcstore.Controllers.Resources.Save;
using pcstore.Core;
using pcstore.Core.Models;
using System.Threading.Tasks;
using System;

namespace pcstore.Controllers
{
    [Authorize]
    [Route("/api/paymentmethods")]
    public class PaymentMethodsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPaymentMethodRepository _repository;
        public PaymentMethodsController(IPaymentMethodRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._repository = repository;
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;

        }

        [HttpGet]
        public async Task<IActionResult> GetPaymentMethods()
        {
            var paymentmethods = await _repository.GetPaymentMethods();
            return Ok(_mapper.Map<IEnumerable<PaymentMethod>, IEnumerable<ViewPaymentMethodResource>>(paymentmethods));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPaymentMethod(int id)
        {
            var paymentmethod = await _repository.GetPaymentMethod(id);
            if (paymentmethod == null) return NotFound();
            return Ok(_mapper.Map<PaymentMethod, ViewPaymentMethodResource>(paymentmethod));
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentMethod([FromBody] SavePaymentMethodResource savePaymentMethodResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var paymentmethod = _mapper.Map<SavePaymentMethodResource, PaymentMethod>(savePaymentMethodResource);
            paymentmethod.Created = DateTime.Now;
            paymentmethod.Modified = DateTime.Now;

            _repository.Add(paymentmethod);

            await _unitOfWork.CompleteAsync();

            paymentmethod = await _repository.GetPaymentMethod(paymentmethod.Id);

            return Ok(_mapper.Map<PaymentMethod, ViewPaymentMethodResource>(paymentmethod));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePaymentMethod(int id, [FromBody] SavePaymentMethodResource savePaymentMethodResource)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);
            var paymentmethod = await _repository.GetPaymentMethod(id);
            if (paymentmethod == null) return NotFound();

            _mapper.Map<SavePaymentMethodResource, PaymentMethod>(savePaymentMethodResource, paymentmethod);
            paymentmethod.Modified = DateTime.Now;

            await _unitOfWork.CompleteAsync();

            paymentmethod = await _repository.GetPaymentMethod(id);

            return Ok(_mapper.Map<PaymentMethod, ViewPaymentMethodResource>(paymentmethod));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePaymentMethod(int id)
        {
            var paymentmethod = await _repository.GetPaymentMethod(id);
            if (paymentmethod == null) return NotFound();
            _repository.Remove(paymentmethod);
            await _unitOfWork.CompleteAsync();
            return Ok(id);
        }
    }
}