using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using pcstore.Controllers;
using pcstore.Controllers.Resources.Save;
using pcstore.Controllers.Resources.View;
using pcstore.Core;
using pcstore.Core.Models;

namespace pcstore.tests.Controllers
{
    [TestFixture]
    public class AddressesControllerTests
    {
        private AddressesController _addressesController;
        private Mock<IAddressRepository> _addressRepository;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IMapper> _mapper;
        private ViewAddressResource _viewAddressResource;
        private Address _addressModel;
        private SaveAddressResource _saveAddressResource;

        [SetUp]
        public void SetUp()
        {
            _viewAddressResource = new ViewAddressResource
            {
                Id = 1,
                FirstName = "Joe",
                LastName = "Black",
                Company = "Fakey",
                Street = "123 Street",
                StreetCont = "cont.",
                PostCode = "1111",
                Suburb = "Middle",
                State = "No Where",
                Country = "Free Country",
                Type = "Invoice"
            };

            _addressModel = new Address
            {
                Id = 1,
                FirstName = "Joe",
                LastName = "Black",
                Company = "Fakey",
                Street = "123 Street",
                StreetCont = "cont.",
                PostCode = "1111",
                Suburb = "Middle",
                State = "No Where",
                Country = "Free Country",
                Type = "Invoice",
                OrderId = 1,
                Created = DateTime.Now,
                Modified = DateTime.Now
            };

            _saveAddressResource = new SaveAddressResource
            {
                FirstName = "Joe",
                LastName = "Black",
                Company = "Fakey",
                Street = "123 Street",
                StreetCont = "cont.",
                PostCode = "1111",
                Suburb = "Middle",
                State = "No Where",
                Country = "Free Country",
                Type = "Invoice",
                OrderId = 1
            };

            _addressRepository = new Mock<IAddressRepository>();

            _unitOfWork = new Mock<IUnitOfWork>();

            _mapper = new Mock<IMapper>();

            _addressesController = new AddressesController(_addressRepository.Object, _mapper.Object, _unitOfWork.Object);
        }

        [Test]
        public async Task GetAddresses_AddressesIsEmpty_ListCountShouldBeZero()
        {
            _mapper
                    .Setup(m => m.Map<IEnumerable<Address>, IEnumerable<ViewAddressResource>>(It.IsAny<IEnumerable<Address>>()))
                    .Returns(new List<ViewAddressResource>());

            _addressRepository.Setup(ar => ar.GetAddresses()).ReturnsAsync(new List<Address>());

            var result = await _addressesController.GetAddresses() as OkObjectResult;

            var addresses = (IEnumerable<ViewAddressResource>)result.Value;

            Assert.That(addresses.Count, Is.EqualTo(0));

        }

        [Test]
        public async Task GetAddresses_AddressesIsNotEmpty_ListCountShouldBeGreaterThanZero()
        {
            _mapper
                    .Setup(m => m.Map<IEnumerable<Address>, IEnumerable<ViewAddressResource>>(It.IsAny<IEnumerable<Address>>()))
                    .Returns(GetViewAddressResources());

            _addressRepository.Setup(ar => ar.GetAddresses()).ReturnsAsync(GetAddresses());

            var result = await _addressesController.GetAddresses() as OkObjectResult;
            var addresses = (IEnumerable<ViewAddressResource>)result.Value;

            Assert.That(addresses.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task GetAddress_InValidId_ReturnNotFound()
        {
            _mapper
                    .Setup(m => m.Map<Address, ViewAddressResource>(It.IsAny<Address>()))
                    .Returns(_viewAddressResource);

            _addressRepository.Setup(ar => ar.GetAddress(0)).ReturnsAsync(GetAddresses().SingleOrDefault(a => a.Id == 0));

            var result = await _addressesController.GetAddress(0) as NotFoundResult;

            Assert.That(result, Is.TypeOf<NotFoundResult>());
        }

        [Test]
        public async Task GetAddress_ValidAddressId_ReturnViewAddressResource()
        {
            _mapper
                    .Setup(m => m.Map<Address, ViewAddressResource>(It.IsAny<Address>()))
                    .Returns(_viewAddressResource);

            _addressRepository.Setup(ar => ar.GetAddress(1)).ReturnsAsync(_addressModel);

            var result = await _addressesController.GetAddress(1) as OkObjectResult;
            var address = (ViewAddressResource)result.Value;

            Assert.That(address, Is.EqualTo(_viewAddressResource));
        }

        [Test]
        [Ignore("to be implemented")]
        public async Task CreateAddress_ValidModelState_ReturnViewAddressResource()
        {
        }

        private IEnumerable<ViewAddressResource> GetViewAddressResources()
        {

            var viewAddresses = new List<ViewAddressResource>()
            {
                new ViewAddressResource
                {
                    Id = 1,
                    FirstName = "Joe",
                    LastName = "Black",
                    Company = "Fakey",
                    Street = "123 Street",
                    StreetCont = "cont.",
                    PostCode = "1111",
                    Suburb = "Middle",
                    State = "No Where",
                    Country = "Free Country",
                    Type = "Invoice"
                },
                new ViewAddressResource
                {
                    Id = 1,
                    FirstName = "Joe",
                    LastName = "Black",
                    Company = "Fakey",
                    Street = "123 Street",
                    StreetCont = "cont.",
                    PostCode = "1111",
                    Suburb = "Middle",
                    State = "No Where",
                    Country = "Free Country",
                    Type = "Invoice"
                }
            };
            return viewAddresses;
        }

        private IEnumerable<Address> GetAddresses()
        {
            var addresses = new List<Address>()
            {
                new Address
                {
                    Id = 1,
                    FirstName = "Joe",
                    LastName = "Black",
                    Company = "Fakey",
                    Street = "123 Street",
                    StreetCont = "cont.",
                    PostCode = "1111",
                    Suburb = "Middle",
                    State = "No Where",
                    Country = "Free Country",
                    Type = "Invoice",
                    OrderId = 1,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                },
                new Address
                {
                    Id = 1,
                    FirstName = "Joe",
                    LastName = "Black",
                    Company = "Fakey",
                    Street = "123 Street",
                    StreetCont = "cont.",
                    PostCode = "1111",
                    Suburb = "Middle",
                    State = "No Where",
                    Country = "Free Country",
                    Type = "Invoice",
                    OrderId = 1,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                }
            };
            return addresses;
        }
    }
}