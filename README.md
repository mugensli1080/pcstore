# Project: PC Store
# Author: Loc Trang
# Date: 26/12/2017

# Requirement
# - MSSQL
# - Dotnet Core SDK 2.0+
# - Node.js

# Instruction

# To run
# root directory has 2 projects: pcstore and pcstore.tests folders. cd into pcstore and follow the instructions below.
# - In terminal / cmd `npm install`
# - In terminal / cmd `dotnet restore`
# - In terminal / cmd `dotnet ef database update`
# - In terminal / cmd `dotnet watch run`